<?php

return [
    'name' => 'Blog',
    'admin' => [
        'sidebar' => [
            [
                'order'  => 3,
                'label'  => 'blog',
                'childs' => [
                    [
                        'label' => 'news',
                        'route' => 'admin.vue-route.blog.news',
                    ],
                    [
                        'label' => 'categories',
                        'route' => 'admin.vue-route.blog.categories',
                    ],
                ],
                'settings' => [
                    [
                        'label' => 'blog',
                        'route' => 'admin.vue-route.blog.settings',
                    ]
                ]
            ],
        ],
    ],
];
