<?php

namespace OctoCmsModule\Blog\Console;

use Exception;
use Illuminate\Console\Command;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use Faker\Generator as Faker;

/**
 * Class FakeBlogCommand
 *
 * @package OctoCmsModule\Blog\Console
 */
class FakeBlogCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fake:blog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /** @var SettingServiceInterface $settingService */
    protected $settingService;
    protected $faker;

    /**
     * FakeBlogCommand constructor.
     *
     * @param SettingServiceInterface $settingService
     * @param Faker                   $faker
     */
    public function __construct(SettingServiceInterface $settingService, Faker $faker)
    {
        parent::__construct();
        $this->settingService = $settingService;
        $this->faker = $faker;
    }


    /**
     * @throws Exception
     */
    public function handle()
    {
        $this->info('Running Fake Blog Command ...');

        $languages = $this->settingService->getSettingByName(SettingNameConst::LANGUAGES);

        $this->info('Creating Categories ...');
        $this->createCategories($languages);

        $this->info('Creating News ...');
        $this->createNews($languages);
    }


    /**
     * @param array $languages
     */
    private function createCategories(array $languages)
    {
        Category::factory()
            ->count(5)
            ->create()
            ->each(function (Category $category) use ($languages) {
                /** @var Page $page */
                $page = Page::factory()->create([
                    'name' => 'Cat-' . $category->id,
                    'type' => Page::TYPE_CATEGORY_NEWS,
                ]);
                $page->pageable()->associate($category)->save();

                foreach ($languages as $lang) {
                    /** @var CategoryLang $categoryLang */
                    $categoryLang = CategoryLang::factory()->create([
                        'lang'        => $lang,
                        'category_id' => $category->id,
                    ]);

                    PageLang::factory()->create([
                        'page_id'    => $page->id,
                        'lang'       => $lang,
                        'meta_title' => $categoryLang->name . '-' . $lang,
                    ]);
                }

                Category::factory()->count(2)->create([
                    'parent_id' => $category->id,
                ])->each(function (Category $subCategory) use ($languages) {

                    /** @var Page $page */
                    $page = Page::factory()->create([
                        'name' => 'Cat-' . $subCategory->id,
                        'type' => Page::TYPE_CATEGORY_NEWS,
                    ]);
                    $page->pageable()->associate($subCategory)->save();

                    foreach ($languages as $lang) {
                        $categoryLang = CategoryLang::factory()->create([
                            'lang'        => $lang,
                            'category_id' => $subCategory->id,
                        ]);

                        PageLang::factory()->create([
                            'page_id'    => $page->id,
                            'lang'       => $lang,
                            'meta_title' => $categoryLang->name . '-' . $lang,
                        ]);
                    }
                });
            });
    }

    /**
     * @param $languages
     */
    private function createNews($languages)
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            News::factory()
                ->count(5)
                ->create()
                ->each(function (News $news) use ($category, $languages) {
                    /** @var Page $page */
                    $page = Page::factory()->make([
                        'name' => 'News-' . $news->id,
                        'type' => Page::TYPE_NEWS,
                    ]);
                    $page->pageable()->associate($news);
                    $page->save();

                    foreach ($languages as $language) {
                        /** @var NewsLang $newsLang */
                        $newsLang = NewsLang::factory()->create([
                            'lang'    => $language,
                            'news_id' => $news->id,
                        ]);

                        PageLang::factory()->create([
                            'page_id'    => $page->id,
                            'lang'       => $language,
                            'meta_title' => $newsLang->name . '-' . $language,
                        ]);
                    }

                    $news->categories()->attach([$category->id]);
                });
        }
    }
}
