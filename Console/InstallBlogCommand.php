<?php

namespace OctoCmsModule\Blog\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;

/**
 * Class InstallCoreCommand
 *
 * @package OctoCmsModule\Core\Console
 * @author  danielepasi
 */
class InstallBlogCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:blog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /** @var SettingServiceInterface $settingService */
    protected $settingService;

    /**
     * InstallServicesCommand constructor.
     *
     * @param SettingServiceInterface $settingService
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        parent::__construct();
        $this->settingService = $settingService;
    }


    public function handle()
    {
        $this->info('Running Install Blog Command ...');

        $languages = $this->settingService->getSettingByName(SettingNameConst::LANGUAGES);

        $this->info('Creating Blog Page ...');

        $this->createBlogPage($languages);

        $this->createSettings($languages);
    }

    /**
     * @param array $langs
     */
    protected function createBlogPage(array $langs)
    {
        $page = new Page([
            'name' => 'Blog',
            'type' => Page::TYPE_BLOG,
        ]);

        $page->save();

        foreach ($langs as $lang) {
            /** @var PageLang $pageLang */
            $pageLang = new PageLang([
                'lang'             => $lang,
                'url'              => 'blog',
                'meta_title'       => env('APP_NAME') . ' - Blog',
                'meta_description' => 'Powered By ' . env('APP_NAME'),
            ]);

            $pageLang->page()->associate($page)->save();
        }
    }

    private function createSettings(array $languages)
    {
        /** @var array $baseSettings */
        $baseSettings = [
            'page_publish' => true,
            'pictures'     => [
                'main' => [
                    'image_width'  => 500,
                    'image_height' => 500,
                ],
            ],
            'base_url'     => [],
        ];

        $data = [
            'news'       => $baseSettings,
            'categories' => $baseSettings,
        ];

        foreach ($data as $entity => $entitySettings) {
            foreach ($languages as $key => $lang) {
                $data[$entity]["base_url"][] = [
                    "value" => $entity,
                    "lang"  => $lang,
                ];
            }
        }

        Setting::updateOrCreate(
            ['name' => 'module_' . strtolower(config('blog.name'))],
            ['value' => $data]
        );
    }
}
