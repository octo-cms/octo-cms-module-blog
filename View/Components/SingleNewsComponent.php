<?php

namespace OctoCmsModule\Blog\View\Components;

use Illuminate\View\Component;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Sitebuilder\Traits\LangValueTrait;

/**
 * Class SingleNewsComponent
 * @package OctoCmsModule\Blog\View\Components
 */
class SingleNewsComponent extends Component
{

    use ImageSrcTrait, LangValueTrait;

    /** @var PageLang */
    public $pageLang;

    /** @var News */
    public $news;

    /**
     * SingleNewsComponent constructor.
     * @param $pageLang
     */
    public function __construct($pageLang)
    {
        $this->pageLang = $pageLang;
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        $this->news = $this->pageLang->page->pageable;

        $this->news
            ->load('newsLangs')
            ->load(
                'newsContents',
                'newsContents.newsContentLangs',
                'newsContents.pictures'
            )
            ->loadPictures();

        return view()->first(
            [
                'blog.components.news',
                strtolower(config('octo-cms.template.module')) . '::blog.components.news',
                'blog::blog.components.news'
            ]
        );
    }
}
