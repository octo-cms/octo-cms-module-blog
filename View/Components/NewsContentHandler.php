<?php

namespace OctoCmsModule\Blog\View\Components;

use Illuminate\View\Component;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Core\Utils\LanguageUtils;

/**
 * Class NewsContentHandler
 *
 * @package OctoCmsModule\Blog\View\Components
 */
class NewsContentHandler extends Component
{
    use ImageSrcTrait;

    /** @var NewsContent */
    public $newsContent;

    /**
     * NewsContentHandler constructor.
     * @param $newsContent
     */
    public function __construct($newsContent)
    {
        $this->newsContent = $newsContent;
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $template = strtolower(config('octo-cms.template.module'));

        switch ($this->newsContent->type) {
            case 'html':
                return view()->first(
                    [
                        'blog.contents.html',
                        $template . '::blog.contents.html'
                    ],
                    [
                        'text' => LanguageUtils::getLangValue($this->newsContent->newsContentLangs, 'text')
                    ]
                );

            case 'picture':
                return view()->first(
                    [
                        'blog.contents.picture',
                        $template . '::blog.contents.picture'
                    ],
                    [
                        'picture' => $this->newsContent->pictures
                            ->where('tag', '=', Picture::TAG_MAIN)
                            ->first()
                    ]
                );
        }
    }
}
