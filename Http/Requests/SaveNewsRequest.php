<?php

namespace OctoCmsModule\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveNewsRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Blog\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SaveNewsRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Arr::collapse([
            [
                'active'                        => 'required|boolean',
                'author'                        => 'required|string',
                'date'                          => 'required',
                'newsLangs'                     => 'required|array',
                'newsLangs.*.lang'              => 'required|string',
                'newsLangs.*.title'             => 'required|string',
                'newsLangs.*.short_description' => 'required|string',
                'tags'                          => 'sometimes|array',
                'categories'                    => 'sometimes|array',
                'categories.*.id'               => 'required_with:categories',
                'categories.*.main'             => 'required_with:categories',
            ],
            $this->getPictureRules(),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'active.required'                        => __('blog::validation.active.required'),
            'active.boolean'                         => __('blog::validation.active.boolean'),
            'author.required'                        => __('blog::validation.author.required'),
            'author.string'                          => __('blog::validation.author.string'),
            'date.required'                          => __('blog::validation.date.required'),
            'newsLangs.required'                     => __('blog::validation.newsLangs.required'),
            'newsLangs.*.lang.required'              => __('blog::validation.newsLangs.*.lang.required'),
            'newsLangs.*.lang.string'                => __('blog::validation.newsLangs.*.lang.string'),
            'newsLangs.*.title.required'             => __('blog::validation.newsLangs.*.title.required'),
            'newsLangs.*.title.string'               => __('blog::validation.newsLangs.*.title.string'),
            'newsLangs.*.short_description.required' => __('blog::validation.newsLangs.*.short_description.required'),
            'newsLangs.*.short_description.string'   => __('blog::validation.newsLangs.*.short_description.string'),
        ];
    }
}
