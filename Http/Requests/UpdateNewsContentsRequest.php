<?php

namespace OctoCmsModule\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveNewsRequest
 *
 * @package OctoCmsModule\Blog\Http\Requests
 */
class UpdateNewsContentsRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return Arr::collapse([
            [
                '*.id'                      => 'present',
                '*.position'                => 'required',
                '*.type'                    => 'required',
                '*.pictures'                => 'sometimes|array',
                '*.videos'                  => 'sometimes|array',
                '*.newsContentLangs'        => 'sometimes|array',
                '*.newsContentLangs.*.lang' => 'sometimes',
                '*.newsContentLangs.*.text' => 'sometimes',
                '*.newImageLangs'           => 'sometimes|array'
            ], $this->getPictureRules('*.')
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
