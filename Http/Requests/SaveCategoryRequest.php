<?php

namespace OctoCmsModule\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveCategoryRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Blog\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SaveCategoryRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Arr::collapse([
            [
                'name'                              => 'sometimes|string',
                'active'                            => 'required',
                'parent_id'                         => 'sometimes',
                'level_depth'                       => 'sometimes',
                'categoryLangs'                     => 'required|array',
                'categoryLangs.*.name'              => 'required|string',
                'categoryLangs.*.lang'              => 'required|string',
                'categoryLangs.*.description'       => 'present',
                'categoryLangs.*.short_description' => 'present',
                'tags'                              => 'sometimes|array',
            ],
            $this->getPictureRules(),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'active.required'               => __('blog::validation.active.required'),
            'active.boolean'                => __('blog::validation.active.boolean'),
            'categoryLangs.required'        => __('blog::validation.categoryLangs.required'),
            'categoryLangs.*.lang.required' => __('blog::validation.categoryLangs.*.lang.required'),
            'categoryLangs.*.lang.string'   => __('blog::validation.categoryLangs.*.lang.string'),
            'categoryLangs.*.name.required' => __('blog::validation.categoryLangs.*.name.required'),
            'categoryLangs.*.name.string'   => __('blog::validation.categoryLangs.*.name.string'),
        ];
    }
}
