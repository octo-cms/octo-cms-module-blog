<?php

namespace OctoCmsModule\Blog\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 *
 * @category Octo
 * @package  OctoCmsModule\Blog\Http\Controllers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteController extends Controller
{
    /**
     * Name news
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function news()
    {

        return view(
            'admin::vue-full-page',
            ['script' => 'blog/news']
        );
    }

    /**
     * Name categories
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function categories()
    {

        return view(
            'admin::vue-full-page',
            ['script' => 'blog/category']
        );
    }

    /**
     * Name newsBuilder
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function newsBuilder()
    {
        return view(
            'admin::vue-full-page',
            ['script' => 'blog/news-builder']
        );
    }

    /**
     * @return mixed
     */
    public function settings()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'blog/settings']);
    }
}
