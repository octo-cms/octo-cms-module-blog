<?php

namespace OctoCmsModule\Blog\Http\Controllers\V1;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Http\Requests\UpdateNewsContentsRequest;
use OctoCmsModule\Blog\Interfaces\PictureServiceInterface;
use OctoCmsModule\Blog\Interfaces\NewsContentServiceInterface;
use OctoCmsModule\Blog\Interfaces\NewsServiceInterface;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Blog\Http\Requests\SaveNewsRequest;
use OctoCmsModule\Blog\Transformers\NewsResource;
use Throwable;

/**
 * Class NewsController
 *
 * @package OctoCmsModule\Blog\Http\Controllers\V1
 */
class NewsController extends Controller
{
    protected $newsService;
    protected $pictureService;
    protected $newsContentService;

    /**
     * NewsController constructor.
     *
     * @param NewsServiceInterface        $newsService
     * @param NewsContentServiceInterface $newsContentService
     * @param PictureServiceInterface     $pictureService
     */
    public function __construct(
        NewsServiceInterface $newsService,
        NewsContentServiceInterface $newsContentService,
        PictureServiceInterface $pictureService
    ) {
        $this->newsService = $newsService;
        $this->pictureService = $pictureService;
        $this->newsContentService = $newsContentService;
    }

    /**
     * @param SaveNewsRequest $request
     *
     * @return JsonResponse|object
     * @throws Throwable
     */
    public function store(SaveNewsRequest $request)
    {
        $fields = $request->validated();

        /** @var News $news */
        $news = $this->newsService
            ->saveNews(
                new News(),
                $fields
            );

        $this->newsService->storeNewsPage($news);

        $this->pictureService->savePicturesEntity($news, Arr::get($fields, 'pictures', []));

        $news->load('newsLangs');

        return (new NewsResource($news))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     */
    public function show($id)
    {
        /** @var News $news */
        $news = News::findOrFail($id);

        return new NewsResource($this->loadFullNewsData($news));
    }

    /**
     * @param SaveNewsRequest $request
     * @param                 $id
     *
     * @return JsonResponse|object
     * @throws Throwable
     */
    public function update(SaveNewsRequest $request, $id)
    {
        $fields = $request->validated();

        /** @var News */
        $news = $this->newsService
            ->saveNews(
                News::findOrFail($id),
                $fields
            );

        $this->pictureService->savePicturesEntity($news, Arr::get($fields, 'pictures', []));

        $news->load('newsLangs');

        return (new NewsResource($news))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     * @throws Exception
     */
    public function delete($id)
    {
        /** @var News $news */
        $news = News::findOrFail($id);
        $news->delete();
        $news->page->delete();

        return response()->json()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    /**
     * @param DatatableRequest          $request
     * @param DatatableServiceInterface $datatableService
     *
     * @return JsonResponse
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        /** @var Builder $news */
        $news = News::query();

        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $news);

        $collection = $datatableDTO->builder->get();

        $collection->load('newsLangs')
            ->load('tagged')
            ->load('categories', 'categories.categoryLangs')
            ->load('pictures', 'pictures.pictureLangs');

        $datatableDTO->collection = NewsResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @param UpdateNewsContentsRequest $request
     * @param                           $id
     *
     * @return NewsResource
     */
    public function updateNewsContents(UpdateNewsContentsRequest $request, $id)
    {
        $news = News::findOrFail($id);

        return new NewsResource($this->loadFullNewsData(
            $this->newsContentService->updateNewsContents(
                $this->loadFullNewsData($news),
                $request->validated()
            )
        ));
    }

    /**
     * @param News $news
     *
     * @return News
     */
    protected function loadFullNewsData(News $news)
    {
        $news->load('newsLangs')
            ->load('page', 'page.pageLangs')
            ->load(
                'newsContents',
                'newsContents.newsContentLangs',
                'newsContents.pictures',
                'newsContents.pictures.pictureLangs',
                'newsContents.videos',
                'newsContents.videos.videoLangs'
            );

        return $news;
    }
}
