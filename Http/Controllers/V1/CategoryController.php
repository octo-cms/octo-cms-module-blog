<?php

namespace OctoCmsModule\Blog\Http\Controllers\V1;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Http\Requests\SelectRequest;
use OctoCmsModule\Core\Services\SelectService;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Blog\Http\Requests\SaveCategoryRequest;
use OctoCmsModule\Blog\Interfaces\CategoryServiceInterface;
use OctoCmsModule\Blog\Interfaces\PictureServiceInterface;
use OctoCmsModule\Blog\Transformers\CategoryResource;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;

/**
 * Class CategoryController
 *
 * @package OctoCmsModule\Blog\Http\Controllers\V1
 */
class CategoryController extends Controller
{
    /** @var CategoryServiceInterface */
    protected $categoryService;

    /** @var PictureServiceInterface */
    protected $pictureService;

    /**
     * CategoryController constructor.
     *
     * @param CategoryServiceInterface $categoryService
     * @param PictureServiceInterface  $pictureService
     */
    public function __construct(
        CategoryServiceInterface $categoryService,
        PictureServiceInterface $pictureService
    ) {
        $this->categoryService = $categoryService;
        $this->pictureService = $pictureService;
    }

    /**
     * @param SaveCategoryRequest $request
     *
     * @return JsonResponse|object
     */
    public function store(SaveCategoryRequest $request)
    {
        /** @var array $fields */
        $fields = $request->validated();

        /** @var Category $category */
        $category = $this->categoryService->saveCategory(new Category(), $request->validated());

        $this->categoryService->storeCategoryPage($category);

        $this->pictureService->savePicturesEntity($category, Arr::get($fields, 'pictures', []));

        $category->load('categoryLangs');

        return (new CategoryResource($category))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param SaveCategoryRequest $request
     * @param                     $id
     *
     * @return JsonResponse|object
     */
    public function update(SaveCategoryRequest $request, $id)
    {
        /** @var array $fields */
        $fields = $request->validated();

        /** @var Category $category */
        $category = $this->categoryService
            ->saveCategory(
                Category::findOrFail($id),
                $fields
            );

        $this->pictureService->savePicturesEntity($category, Arr::get($fields, 'pictures', []));

        $category->load('categoryLangs');

        return (new CategoryResource($category))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     * @throws Exception
     */
    public function delete($id)
    {
        /** @var Category $category */
        $category = Category::findOrFail($id);
        $category->delete();
        optional($category->page)->delete();

        return response()->json()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    /**
     * @param DatatableRequest          $request
     * @param DatatableServiceInterface $datatableService
     *
     * @return JsonResponse
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        /** @var Builder $brands */
        $categories = Category::query();

        /** @var array $fields */
        $fields = $request->validated();

        /** @var string $query */
        $query = !empty(Arr::get($fields, 'query', ''))
            ? Arr::get($fields, 'query', '')
            : '';

        $datatableDTO = $datatableService->getDatatableDTO(
            $fields,
            $this->categoryService->filterCategoryBuilder(
                $categories,
                Arr::get($fields, 'filters', []),
                $query
            )
        );

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('parents')
            ->load('children')
            ->load('categoryLangs')
            ->load('tagged')
            ->load('pictures', 'pictures.pictureLangs');

        $datatableDTO->collection = CategoryResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function showTree()
    {
        /** @var Builder $brands */
        $categories = Category::getTree();

        $categories->load('children');

        return CategoryResource::collection(Category::getTree());
    }

    /**
     * @param SelectRequest           $request
     * @param SelectService           $selectService
     * @param SettingServiceInterface $settingService
     *
     * @return JsonResponse|object
     */
    public function selectIndex(
        SelectRequest $request,
        SelectService $selectService,
        SettingServiceInterface $settingService
    ) {
        /** @var array $fields */
        $fields = $request->validated();
        /** @var string $query */
        $query = Arr::get($fields, 'query', '');

        /** @var string $locale */
        $locale = $settingService->getSettingByName(SettingNameConst::LOCALE) ?: 'it';

        /** @var Collection $categoryLangs */
        $categoryLangs = CategoryLang::where('name', 'like', "%$query%")
            ->where('lang', '=', $locale)
            ->orderBy('name')
            ->get();

        return response()
            ->json($selectService->parseCollection($categoryLangs, 'name', 'category_id'))
            ->setStatusCode(Response::HTTP_OK);
    }
}
