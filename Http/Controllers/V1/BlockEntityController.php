<?php

namespace OctoCmsModule\Blog\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Http\Requests\EntityIdsRequest;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Interfaces\EntityIdsServiceInterface;

/**
 * Class BlockEntityController
 *
 * @package OctoCmsModule\Blog\Http\Controllers\V1
 */
class BlockEntityController extends Controller
{


    /**
     * @param EntityIdsRequest          $request
     * @param EntityIdsServiceInterface $entityIdsService
     * @return JsonResponse
     */
    public function blockEntityIds(
        EntityIdsRequest $request,
        EntityIdsServiceInterface $entityIdsService
    ) {
        $fields = $request->validated();

        /** @var EntityIdsDTO $entityIdsDTO */
        $entityIdsDTO = $entityIdsService->getEntityIdsDTO(
            News::with('newsLangs')->whereNotIn('id', Arr::get($fields, 'excludedIds', [])),
            [
                'orderBy'     => 'author',
                'currentPage' => Arr::get($fields, 'currentPage', 1),
                'rowsInPage'  => Arr::get($fields, 'rowsInPage', 12)
            ]
        );

        $entityIdsDTO->collection = $entityIdsService->parseMultiLangCollection(
            $entityIdsDTO->builder->get(),
            'news_langs',
            'id',
            'title',
            'short_description'
        );

        return response()->json($entityIdsDTO, Response::HTTP_OK);
    }
}
