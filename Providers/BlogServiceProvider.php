<?php

namespace OctoCmsModule\Blog\Providers;

use Config;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use OctoCmsModule\Blog\Console\FakeBlogCommand;
use OctoCmsModule\Blog\Console\InstallBlogCommand;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Interfaces\CategoryServiceInterface;
use OctoCmsModule\Blog\Interfaces\PictureServiceInterface;
use OctoCmsModule\Blog\Interfaces\NewsContentServiceInterface;
use OctoCmsModule\Blog\Interfaces\NewsServiceInterface;
use OctoCmsModule\Blog\Tests\Mocks\Services\CategoryServiceMock;
use OctoCmsModule\Blog\Tests\Mocks\Services\PictureServiceMock;
use OctoCmsModule\Blog\Tests\Mocks\Services\NewsContentServiceMock;
use OctoCmsModule\Blog\Tests\Mocks\Services\NewsServiceMock;

/**
 *
 * Class BlogServiceProvider
 * @package OctoCmsModule\Blog\Providers
 */
class BlogServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Blog';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'blog';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));

        $this->commands([
            InstallBlogCommand::class,
            FakeBlogCommand::class,
        ]);

        Blade::component(
            'news-content-handler',
            getBindingImplementation($this->moduleName, "View\Components\NewsContentHandler")
        );

        Blade::component(
            'single-news-component',
            getBindingImplementation(
                [config('octo-cms.template.module', null), $this->moduleName],
                "View\Components\SingleNewsComponent")
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        if ($this->app->environment() === 'testing') {
            $this->app->bind(NewsServiceInterface::class, NewsServiceMock::class);
            $this->app->bind(PictureServiceInterface::class, PictureServiceMock::class);
            $this->app->bind(CategoryServiceInterface::class, CategoryServiceMock::class);
            $this->app->bind(NewsContentServiceInterface::class, NewsContentServiceMock::class);
        } else {

            $this->app->bind(
                NewsServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\NewsService")
            );

            $this->app->bind(
                PictureServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\PictureService")
            );
            $this->app->bind(
                CategoryServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\CategoryService")
            );
            $this->app->bind(
                NewsContentServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\NewsContentService")
            );

        }

        Relation::morphMap([
            'CategoryNews' => Category::class,
            'News'         => News::class,
            'NewsContent'  => NewsContent::class,
        ]);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath,
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
