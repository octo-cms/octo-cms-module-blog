<?php

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {
//    Route::group(['middleware' => []], function () {

        Route::group(['prefix' => 'blog'], function () {

            Route::group(['prefix' => 'news'], function () {

                Route::post(
                    'block-entity-ids',
                    getRouteAction("Blog", "V1\BlockEntityController", 'blockEntityIds')
                )
                    ->name('admin.blog.block.entity.ids');

                Route::post('', getRouteAction("Blog", "V1\NewsController", 'store'))
                    ->name('admin.blog.news.store');

                Route::group(['prefix' => '{id}'], function () {
                    Route::get('', getRouteAction("Blog", "V1\NewsController", 'show'))
                        ->name('admin.blog.news.show');
                    Route::delete('', getRouteAction("Blog", "V1\NewsController", 'delete'))
                        ->name('admin.blog.news.delete');
                    Route::put('', getRouteAction("Blog", "V1\NewsController", 'update'))
                        ->name('admin.blog.news.update');
                    Route::put('contents', getRouteAction("Blog", "V1\NewsController", 'updateNewsContents'))
                        ->name('admin.blog.news.update.contents');
                });
            });

            Route::group(['prefix' => 'categories'], function () {
                Route::get('show-tree', getRouteAction("Blog", "V1\CategoryController", 'showTree'))
                    ->name('admin.blog.categories.show-tree');
                Route::post('', getRouteAction("Blog", "V1\CategoryController", 'store'))
                    ->name('admin.blog.categories.store');

                Route::group(['prefix' => '{id}'], function () {
                    Route::delete('', getRouteAction("Blog", "V1\CategoryController", 'delete'))
                        ->name('admin.blog.categories.delete');
                    Route::put('', getRouteAction("Blog", "V1\CategoryController", 'update'))
                        ->name('admin.blog.categories.update');
                });
            });
        });

        Route::group(['prefix' => 'datatables/blog'], function () {
            Route::post(
                'news',
                getRouteAction("Blog", "V1\NewsController", 'datatableIndex')
            )->name('admin.datatables.news');

            Route::post(
                'categories',
                getRouteAction("Blog", "V1\CategoryController", 'datatableIndex')
            )->name('admin.datatables.categories.news');
        });

        Route::group(['prefix' => 'select'], function () {
            Route::post(
                'categories-news',
                getRouteAction("Blog", "V1\CategoryController", 'selectIndex')
            )->name('admin.select.categories.news.index');
        });
    });
});
