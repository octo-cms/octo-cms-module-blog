<?php


Route::group(['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']], function () {
    Route::group(['prefix' => 'blog'], function () {

        Route::get(
            'news',
            getRouteAction("Blog", "VueRouteController", 'news')
        )
            ->name('admin.vue-route.blog.news');

        Route::get(
            'categories',
            getRouteAction("Blog", "VueRouteController", 'categories')
        )
            ->name('admin.vue-route.blog.categories');

        Route::get(
            'news-builder/{id}',
            getRouteAction("Blog", "VueRouteController", 'newsBuilder')
        )
            ->name('admin.vue-route.blog.news-builder');

        Route::get(
            'settings',
            getRouteAction("Blog", "VueRouteController", 'settings')
        )
            ->name('admin.vue-route.blog.settings');
    });
});
