<?php

namespace OctoCmsModule\Blog\Interfaces;

use OctoCmsModule\Core\Interfaces\PictureServiceInterface as PictureServiceInterfaceCore;

/**
 * Interface PictureServiceInterface
 *
 * @package OctoCmsModule\Blog\Interfaces
 */
interface PictureServiceInterface extends PictureServiceInterfaceCore
{

}
