<?php

namespace OctoCmsModule\Blog\Interfaces;

use OctoCmsModule\Blog\Entities\News;

/**
 * Interface NewsContentServiceInterface
 * @package OctoCmsModule\Blog\Interfaces
 */
interface NewsContentServiceInterface
{
    /**
     * @param News  $news
     * @param array $fields
     *
     * @return News
     */
    public function updateNewsContents(News $news, array $fields): News;
}
