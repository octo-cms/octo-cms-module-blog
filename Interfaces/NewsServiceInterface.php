<?php

namespace OctoCmsModule\Blog\Interfaces;

use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Interface NewsServiceInterface
 *
 * @package OctoCmsModule\Blog\Interfaces
 */
interface NewsServiceInterface
{
    /**
     * @param News  $news
     * @param array $fields
     *
     * @return News
     */
    public function saveNews(News $news, array $fields): News;

    /**
     * @param News $news
     *
     * @return Page
     */
    public function storeNewsPage(News $news): Page;
}
