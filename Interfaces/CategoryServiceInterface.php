<?php

namespace OctoCmsModule\Blog\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Interface CategoryServiceInterface
 *
 * @package OctoCmsModule\Blog\Interfaces
 */
interface CategoryServiceInterface
{
    /**
     * @param Category $category
     * @param array    $fields
     *
     * @return Category
     */
    public function saveCategory(Category $category, array $fields): Category;

    /**
     * @param Category $category
     *
     * @return Page
     */
    public function storeCategoryPage(Category $category): Page;

    /**
     * @param Builder $categories
     * @param array   $filters
     * @param string  $query
     *
     * @return Builder
     */
    public function filterCategoryBuilder(Builder $categories, array $filters = [], string $query = ''): Builder;
}
