<?php

namespace OctoCmsModule\Blog\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Blog\Entities\News;

/**
 * Class NewsFactory
 *
 * @package OctoCmsModule\Blog\Factories
 */
class NewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'active' => true,
            'author' => $this->faker->name,
            'date'   => Carbon::now()->format('Y-m-d'),
        ];
    }
}
