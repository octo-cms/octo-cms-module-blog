<?php

namespace OctoCmsModule\Blog\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;

/**
 * Class CategoryLangFactory
 *
 * @package OctoCmsModule\Blog\Factories
 */
class CategoryLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoryLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id'       => Category::factory(),
            'lang'              => $this->faker->randomElement(['it', 'en']),
            'name'              => $this->faker->slug(3),
            'description'       => $this->faker->text(400),
            'short_description' => $this->faker->text(200),
        ];
    }
}
