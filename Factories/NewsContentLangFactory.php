<?php

namespace OctoCmsModule\Blog\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Entities\NewsContentLang;

/**
 * Class NewsContentLangFactory
 *
 * @package OctoCmsModule\Blog\Factories
 */
class NewsContentLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NewsContentLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'news_content_id' => NewsContent::factory(),
            'lang'            => $this->faker->randomElement(['it', 'en']),
            'text'            => $this->faker->text(200),
        ];
    }
}
