<?php

namespace OctoCmsModule\Blog\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsContent;

/**
 * Class NewsContentFactory
 *
 * @package OctoCmsModule\Blog\Factories
 */
class NewsContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NewsContent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'news_id'  => News::factory(),
            'position' => $this->faker->numberBetween(0, 10),
            'type'     => $this->faker->randomElement([
                NewsContent::TYPE_HTML,
                NewsContent::TYPE_PICTURE,
                NewsContent::TYPE_GALLERY,
                NewsContent::TYPE_VIDEO,
            ]),
        ];
    }
}
