<?php

namespace OctoCmsModule\Blog\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Blog\Entities\Category;

/**
 * Class CategoryFactory
 *
 * @package OctoCmsModule\Blog\Factories
 */
class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'active' => true,
        ];
    }
}
