<?php

namespace OctoCmsModule\Blog\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;

/**
 * Class NewsLangFactory
 *
 * @package OctoCmsModule\Blog\Factories
 */
class NewsLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NewsLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'news_id'           => News::factory(),
            'lang'              => $this->faker->randomElement(['it', 'en']),
            'title'             => $this->faker->slug(3),
            'short_description' => $this->faker->text(200),
        ];
    }
}
