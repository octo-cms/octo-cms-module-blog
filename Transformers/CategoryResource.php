<?php

namespace OctoCmsModule\Blog\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;
use OctoCmsModule\Core\Transformers\TagResource;

/**
 * Class CategoryResource
 *
 * @package OctoCmsModule\Blog\Transformers
 */
class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'parent_id'     => $this->parent_id,
            'level_depth'   => $this->level_depth,
            'active'        => $this->active,
            'categoryLangs' => CategoryLangResource::collection($this->whenLoaded('categoryLangs')),
            'parents'       => new CategoryResource($this->whenLoaded('parents')),
            'children'      => CategoryResource::collection($this->whenLoaded('children')),
            'main'          => $this->whenPivotLoaded('blog_category_news', function () {
                return $this->pivot->main;
            }),
            'tags'          => TagResource::collection($this->whenLoaded('tagged')),
            'pictures'      => PictureResource::collection($this->whenLoaded('pictures')),
        ];
    }
}
