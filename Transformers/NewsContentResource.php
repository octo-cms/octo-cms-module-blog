<?php

namespace OctoCmsModule\Blog\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;
use OctoCmsModule\Core\Transformers\VideoResource;

/**
 * Class NewsContentResource
 *
 * @package OctoCmsModule\Blog\Transformers
 */
class NewsContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'news_id'          => $this->news_id,
            'position'         => $this->position,
            'type'             => $this->type,
            'news'             => new NewsResource($this->whenLoaded('news')),
            'newsContentLangs' => NewsContentLangResource::collection($this->whenLoaded('newsContentLangs')),
            'pictures'         => PictureResource::collection($this->whenLoaded('pictures')),
            'videos'           => VideoResource::collection($this->whenLoaded('videos')),
        ];
    }
}
