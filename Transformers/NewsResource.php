<?php

namespace OctoCmsModule\Blog\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;
use OctoCmsModule\Core\Transformers\TagResource;
use OctoCmsModule\Sitebuilder\Transformers\PageResource;

/**
 * Class NewsResource
 *
 * @package OctoCmsModule\Blog\Transformers
 */
class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'author'       => $this->author,
            'active'       => $this->active,
            'date'         => optional($this->date)->format('Y-m-d'),
            'related'      => NewsResource::collection($this->whenLoaded('related')),
            'newsLangs'    => NewsLangResource::collection($this->whenLoaded('newsLangs')),
            'newsContents' => NewsContentResource::collection($this->whenLoaded('newsContents')),
            'pictures'     => PictureResource::collection($this->whenLoaded('pictures')),
            'page'         => new PageResource($this->whenLoaded('page')),
            'tags'         => TagResource::collection($this->whenLoaded('tagged')),
            'categories'   => CategoryResource::collection($this->whenLoaded('categories')),
        ];
    }
}
