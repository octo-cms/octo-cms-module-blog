<?php

namespace OctoCmsModule\Blog\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CategoryLangResource
 *
 * @package OctoCmsModule\Blog\Transformers
 */
class CategoryLangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'category_id'       => $this->category_id,
            'name'              => $this->name,
            'lang'              => $this->lang,
            'description'       => $this->description,
            'short_description' => $this->short_description,
        ];
    }
}
