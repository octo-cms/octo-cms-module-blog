<?php

namespace OctoCmsModule\Blog\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NewsLangResource
 *
 * @package OctoCmsModule\Blog\Transformers
 */
class NewsLangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lang'              => $this->lang,
            'short_description' => $this->short_description,
            'title'             => $this->title,
            'news'              => new NewsResource($this->whenLoaded('news')),
        ];
    }
}
