<?php

namespace OctoCmsModule\Blog\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NewsContentLangResource
 *
 * @package OctoCmsModule\Blog\Transformers
 */
class NewsContentLangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lang'            => $this->lang,
            'text'            => $this->text,
            'newsContent'     => new NewsResource($this->whenLoaded('news')),
        ];
    }
}
