<?php

namespace OctoCmsModule\Blog\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use OctoCmsModule\Blog\Factories\NewsContentLangFactory;

/**
 * OctoCmsModule\Blog\Entities\NewsContentLang
 *
 * @property int                             $id
 * @property int                             $news_content_id
 * @property string                          $lang
 * @property string|null                     $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read NewsContent                $newsContent
 * @method static Builder|NewsContentLang newModelQuery()
 * @method static Builder|NewsContentLang newQuery()
 * @method static Builder|NewsContentLang query()
 * @method static Builder|NewsContentLang whereCreatedAt($value)
 * @method static Builder|NewsContentLang whereId($value)
 * @method static Builder|NewsContentLang whereLang($value)
 * @method static Builder|NewsContentLang whereNewsContentId($value)
 * @method static Builder|NewsContentLang whereText($value)
 * @method static Builder|NewsContentLang whereUpdatedAt($value)
 * @mixin Eloquent
 */
class NewsContentLang extends Model
{
    use HasFactory;

    protected $fillable = [
        'news_content_id',
        'lang',
        'text'
    ];

    protected $table = 'blog_news_content_langs';

    /**
     * @return NewsContentLangFactory
     */
    protected static function newFactory()
    {
        return NewsContentLangFactory::new();
    }

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function newsContent()
    {
        return $this->belongsTo(NewsContent::class);
    }
}
