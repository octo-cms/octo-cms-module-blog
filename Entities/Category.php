<?php

namespace OctoCmsModule\Blog\Entities;

use Conner\Tagging\Model\Tagged;
use Conner\Tagging\Taggable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Traits\PageableTrait;
use OctoCmsModule\Blog\Factories\CategoryFactory;
use OctoCmsModule\Core\Traits\PicturableTrait;

/**
 * OctoCmsModule\Blog\Entities\Category
 *
 * @property int                                                     $id
 * @property int|null                                                $parent_id
 * @property bool                                                    $active
 * @property Carbon|null                    $created_at
 * @property Carbon|null                    $updated_at
 * @property-read Collection|CategoryLang[] $categoryLangs
 * @property-read int|null                  $category_langs_count
 * @property-read Collection|Category[]     $children
 * @property-read int|null                  $children_count
 * @property-read Page|null                 $page
 * @property-read Category|null             $parent
 * @property-read Category|null             $parents
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|Category onlyTrashed()
 * @method static Builder|Category query()
 * @method static Builder|Category whereActive($value)
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereParentId($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withoutTrashed()
 * @mixin Eloquent
 * @property Carbon|null               $deleted_at
 * @method static Builder|Category whereDeletedAt($value)
 * @property array                     $tag_names
 * @property-read Collection|\Tagged[] $tags
 * @property-read Collection|Tagged[]  $tagged
 * @property-read int|null             $tagged_count
 * @method static Builder|Category withAllTags($tagNames)
 * @method static Builder|Category withAnyTag($tagNames)
 * @method static Builder|Category withoutTags($tagNames)
 * @property-read Collection|Picture[] $pictures
 * @property-read int|null             $pictures_count
 */
class Category extends Model
{
    use PageableTrait, SoftDeletes, Taggable, PicturableTrait, HasFactory;

    public const GCS_PATH = 'categories';

    protected $table = 'blog_categories';

    protected $fillable = [
        'name',
        'active',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * @return CategoryFactory
     */
    protected static function newFactory()
    {
        return CategoryFactory::new();
    }

    /**
     * @return HasMany
     */
    public function categoryLangs()
    {
        return $this->hasMany(CategoryLang::class);
    }

    /**
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * @return HasOne
     */
    public function parents()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id')
            ->with('categoryLangs')
            ->with('parents');
    }

    /**
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')
            ->with('categoryLangs')
            ->with('children');
    }

    /**
     * @param null $parentId
     *
     * @return Builder[]|Collection
     */
    public static function getTree($parentId = null)
    {
        return Category::with('children')
            ->with('categoryLangs')
            ->where('parent_id', '=', $parentId)
            ->get();
    }
}
