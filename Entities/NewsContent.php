<?php

namespace OctoCmsModule\Blog\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Blog\Factories\NewsContentFactory;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\Video;
use OctoCmsModule\Core\Traits\PicturableTrait;
use OctoCmsModule\Core\Traits\VideableTrait;

/**
 * OctoCmsModule\Blog\Entities\NewsContent
 *
 * @property int                               $id
 * @property int                               $news_id
 * @property int                               $position
 * @property string                            $type
 * @property Carbon|null   $created_at
 * @property Carbon|null   $updated_at
 * @property-read News                         $news
 * @property-read Collection|NewsContentLang[] $newsContentLangs
 * @property-read int|null                     $news_content_langs_count
 * @property-read Collection|Picture[]         $pictures
 * @property-read int|null                     $pictures_count
 * @property-read Collection|Video[]           $videos
 * @property-read int|null                     $videos_count
 * @method static Builder|NewsContent newModelQuery()
 * @method static Builder|NewsContent newQuery()
 * @method static Builder|NewsContent query()
 * @method static Builder|NewsContent whereCreatedAt($value)
 * @method static Builder|NewsContent whereId($value)
 * @method static Builder|NewsContent whereNewsId($value)
 * @method static Builder|NewsContent wherePosition($value)
 * @method static Builder|NewsContent whereType($value)
 * @method static Builder|NewsContent whereUpdatedAt($value)
 * @mixin Eloquent
 */
class NewsContent extends Model
{
    use PicturableTrait, VideableTrait, HasFactory;

    public const TYPE_HTML = 'html';
    public const TYPE_PICTURE = 'picture';
    public const TYPE_GALLERY = 'gallery';
    public const TYPE_VIDEO = 'video';

    public const GCS_PATH = 'news-contents';

    protected $fillable = [
        'position',
        'type'
    ];

    protected $table = 'blog_news_contents';

    /**
     * @return NewsContentFactory
     */
    protected static function newFactory()
    {
        return NewsContentFactory::new();
    }

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function news()
    {
        return $this->belongsTo(News::class);
    }

    /**
     * @return HasMany
     */
    public function newsContentLangs()
    {
        return $this->hasMany(NewsContentLang::class);
    }
}
