<?php

namespace OctoCmsModule\Blog\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Blog\Factories\CategoryLangFactory;
use OctoCmsModule\Sitebuilder\Traits\SlugTrait;

/**
 * OctoCmsModule\Blog\Entities\CategoryLang
 *
 * @property int           $id
 * @property int           $category_id
 * @property string        $lang
 * @property string        $name
 * @property string|null   $short_description
 * @property string|null   $description
 * @property-read Category $category
 * @method static Builder|CategoryLang newModelQuery()
 * @method static Builder|CategoryLang newQuery()
 * @method static Builder|CategoryLang query()
 * @method static Builder|CategoryLang whereCategoryId($value)
 * @method static Builder|CategoryLang whereDescription($value)
 * @method static Builder|CategoryLang whereId($value)
 * @method static Builder|CategoryLang whereLang($value)
 * @method static Builder|CategoryLang whereName($value)
 * @method static Builder|CategoryLang whereShortDescription($value)
 * @mixin Eloquent
 * @property string        $slug
 * @method static Builder|CategoryLang whereSlug($value)
 */
class CategoryLang extends Model
{
    use SlugTrait, HasFactory;

    protected $table = 'blog_category_langs';

    public $timestamps = false;

    protected $fillable = [
        'category_id',
        'lang',
        'name',
        'short_description',
        'description',
    ];

    /**
     * @return CategoryLangFactory
     */
    protected static function newFactory()
    {
        return CategoryLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = $this->retrieveSlug($value);
    }
}
