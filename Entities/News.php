<?php

namespace OctoCmsModule\Blog\Entities;

use Conner\Tagging\Model\Tagged;
use Conner\Tagging\Taggable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Blog\Factories\NewsFactory;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Sitebuilder\Traits\PageableTrait;
use OctoCmsModule\Core\Traits\PicturableTrait;

/**
 * OctoCmsModule\Blog\Entities\News
 *
 * @property int                             $id
 * @property bool                            $active
 * @property string                          $author
 * @property Carbon      $date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Category[]      $categories
 * @property-read int|null                   $categories_count
 * @property-read Collection|NewsLang[]      $newsLangs
 * @property-read int|null                   $news_langs_count
 * @property-read Page|null                  $page
 * @property-read Collection|Picture[]       $pictures
 * @property-read int|null                   $pictures_count
 * @method static Builder|News newModelQuery()
 * @method static Builder|News newQuery()
 * @method static Builder|News query()
 * @method static Builder|News whereActive($value)
 * @method static Builder|News whereAuthor($value)
 * @method static Builder|News whereCreatedAt($value)
 * @method static Builder|News whereDate($value)
 * @method static Builder|News whereId($value)
 * @method static Builder|News whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|NewsContent[] $newsContents
 * @property-read int|null                 $news_contents_count
 * @property-read Collection|News[]        $relateds
 * @property-read int|null                 $relateds_count
 * @property array                         $tag_names
 * @property-read Collection|\Tagged[]     $tags
 * @property-read Collection|Tagged[]      $tagged
 * @property-read int|null                 $tagged_count
 * @method static Builder|News withAllTags($tagNames)
 * @method static Builder|News withAnyTag($tagNames)
 * @method static Builder|News withoutTags($tagNames)
 */
class News extends Model
{
    use PageableTrait, PicturableTrait, Taggable, HasFactory;

    public const GCS_PATH = 'news';

    protected $fillable = [
        'active',
        'author',
        'date',
    ];

    protected $casts = [
        'active' => 'boolean',
        'date'   => 'date',
    ];

    protected $table = 'blog_news';

    /**
     * @return NewsFactory
     */
    protected static function newFactory()
    {
        return NewsFactory::new();
    }

    /**
     * @return HasMany
     */
    public function newsLangs()
    {
        return $this->hasMany(NewsLang::class);
    }

    /**
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'blog_category_news')
            ->withPivot('main');
    }

    /**
     * @return BelongsToMany
     */
    public function relateds()
    {
        return $this->belongsToMany(
            News::class,
            'blog_news_related',
            'news_id',
            'related_id'
        );
    }

    /**
     * @return HasMany
     */
    public function newsContents()
    {
        return $this->hasMany(NewsContent::class)->orderBy("position");
    }
}
