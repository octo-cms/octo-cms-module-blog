<?php

namespace OctoCmsModule\Blog\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use OctoCmsModule\Blog\Factories\NewsLangFactory;

/**
 * OctoCmsModule\Blog\Entities\NewsLang
 *
 * @property int                             $id
 * @property int                             $news_id
 * @property string                          $lang
 * @property string                          $title
 * @property string                          $short_description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read News                       $news
 * @method static Builder|NewsLang newModelQuery()
 * @method static Builder|NewsLang newQuery()
 * @method static Builder|NewsLang query()
 * @method static Builder|NewsLang whereCreatedAt($value)
 * @method static Builder|NewsLang whereId($value)
 * @method static Builder|NewsLang whereLang($value)
 * @method static Builder|NewsLang whereNewsId($value)
 * @method static Builder|NewsLang whereShortDescription($value)
 * @method static Builder|NewsLang whereTitle($value)
 * @method static Builder|NewsLang whereUpdatedAt($value)
 * @mixin Eloquent
 */
class NewsLang extends Model
{
    use HasFactory;

    protected $fillable = [
        'news_id',
        'lang',
        'title',
        'short_description',
    ];

    protected $table = 'blog_news_langs';

    /**
     * @return NewsLangFactory
     */
    protected static function newFactory()
    {
        return NewsLangFactory::new();
    }

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
