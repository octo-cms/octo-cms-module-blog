<?php

namespace OctoCmsModule\Blog\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Blog\Interfaces\CategoryServiceInterface;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Class CategoryService
 *
 * @package OctoCmsModule\Blog\Services
 */
class CategoryService implements CategoryServiceInterface
{
    /**
     * @param Category $category
     * @param array    $fields
     *
     * @return Category
     */
    public function saveCategory(Category $category, array $fields): Category
    {
        $category->fill($fields);

        if ($parentCategory = Category::find(Arr::get($fields, 'parent_id', ''))) {
            $category->parent()->associate($parentCategory);
        }

        $category->save();

        $category->retag(Arr::get($fields, 'tags', []));

        foreach (Arr::get($fields, 'categoryLangs', []) as $categoryLang) {
            CategoryLang::updateOrCreate(
                [
                    'category_id' => $category->id,
                    'lang'        => Arr::get($categoryLang, 'lang', ''),
                ],
                [
                    'name'              => Arr::get($categoryLang, 'name', ''),
                    'description'       => Arr::get($categoryLang, 'description', ''),
                    'short_description' => Arr::get($categoryLang, 'short_description', ''),
                ]
            );
        }

        return $category;
    }

    /**
     * @param Category $category
     *
     * @return Page
     */
    public function storeCategoryPage(Category $category): Page
    {
        $pageName = LanguageUtils::getLangValue($category->categoryLangs, 'name');

        /** @var Page $page */
        $page = new Page([
            'name' => !empty($pageName) ? $pageName : (Page::TYPE_CATEGORY_NEWS . '-' . $category->id),
            'type' => Page::TYPE_CATEGORY_NEWS,
        ]);

        $page->pageable()->associate($category)->save();

        foreach ($category->categoryLangs as $categoryLang) {
            $pageLang = new PageLang([
                'url'              => PageLang::PREFIX_CATEGORIES . $categoryLang->slug,
                'lang'             => $categoryLang->lang,
                'meta_title'       => $categoryLang->name,
                'meta_description' => $categoryLang->description,
            ]);

            $pageLang->page()->associate($page)->save();
        }

        return $page;
    }

    /**
     * @param Builder $categories
     * @param array   $filters
     * @param string  $query
     *
     * @return Builder
     */
    public function filterCategoryBuilder(Builder $categories, array $filters = [], string $query = ''): Builder
    {
        if (Arr::has($filters, 'active')) {
            $categories->where('active', '=', Arr::get($filters, 'active', true));
        }

        if (Arr::has($filters, 'parent_id')) {
            $categories
                ->where('parent_id', '=', Arr::get($filters, 'parent_id', true));
        }

        if (!empty($query)) {
            $categories->WhereHas('categoryLangs', function (Builder $categoryLangBuilder) use ($query) {
                $categoryLangBuilder
                    ->where('name', 'LIKE', '%' . $query . '%')
                    ->orWhere('description', 'LIKE', '%' . $query . '%')
                    ->orWhere('short_description', 'LIKE', '%' . $query . '%');
            });
        }

        return $categories;
    }
}
