<?php

namespace OctoCmsModule\Blog\Services;

use DB;
use Illuminate\Support\Arr;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Entities\NewsContentLang;
use OctoCmsModule\Blog\Interfaces\NewsContentServiceInterface;
use OctoCmsModule\Blog\Interfaces\PictureServiceInterface;
use OctoCmsModule\Core\Exceptions\OctoCmsException;

/**
 * Class NewsService
 *
 * @package OctoCmsModule\Blog\Services
 */
class NewsContentService implements NewsContentServiceInterface
{

    protected $pictureService;

    /**
     * NewsContentService constructor.
     * @param PictureServiceInterface $pictureService
     */
    public function __construct(
        PictureServiceInterface $pictureService
    ) {
        $this->pictureService = $pictureService;
    }

    /**
     * @param News  $news
     * @param array $fields
     * @return News
     * @throws OctoCmsException
     * @throws \Throwable
     */
    public function updateNewsContents(News $news, array $fields): News
    {

        DB::beginTransaction();
        // remove deleted contents
        $news->newsContents()
            ->whereIn(
                'id',
                array_diff(
                    $news->newsContents->pluck('id')->toArray(),
                    Arr::pluck($fields, 'id')
                )
            )->delete();

        foreach ($fields as $content) {
            $newsContent = $news
                ->newsContents
                ->find(Arr::get($content, 'id', null));

            if (empty($newsContent)) {
                /** @var NewsContent */
                $newsContent = new NewsContent();
                $newsContent->news()->associate($news);
            }

            $newsContent->fill($content);
            $newsContent->save();

            if (!$this->saveNewsContentsData($newsContent, $content)) {
                DB::rollBack();
                throw new OctoCmsException('content not saved');
            };
        }

        DB::commit();
        return $news;
    }

    /**
     * @param NewsContent $newsContent
     * @param array       $content
     * @return false
     */
    protected function saveNewsContentsData(NewsContent $newsContent, array $content)
    {
        switch ($newsContent->type) {
            case NewsContent::TYPE_HTML:
                return $this->saveNewsContentDataHtml($newsContent, $content);

            case NewsContent::TYPE_PICTURE:
                return $this->saveNewsContentDataPicture($newsContent, $content);

            case NewsContent::TYPE_GALLERY:
            case NewsContent::TYPE_VIDEO:
                return true;

            default:
                return false;
        }
    }

    /**
     * @param NewsContent $newsContent
     * @param array       $content
     * @return bool
     */
    protected function saveNewsContentDataHtml(NewsContent $newsContent, array $content)
    {

        foreach (Arr::get($content, 'newsContentLangs', []) as $newsContentLang) {
            $lang = Arr::get($newsContentLang, 'lang', null);
            $text = Arr::get($newsContentLang, 'text', null);

            if (empty($lang)) {
                return false;
            }

            NewsContentLang::updateOrCreate([
                'news_content_id' => $newsContent->id,
                'lang'            => $lang
            ], [
                'text' => $text
            ]);
        }

        return true;
    }

    /**
     * @param       $entity
     * @param array $content
     * @return bool
     */
    protected function saveNewsContentDataPicture($entity, array $content)
    {
        return $this->pictureService->savePicturesEntity($entity, Arr::get($content, 'pictures', []));
    }
}
