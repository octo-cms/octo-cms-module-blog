<?php

namespace OctoCmsModule\Blog\Services;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Blog\Interfaces\NewsServiceInterface;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use Facades\OctoCmsModule\Core\Services\SettingService;

/**
 * Class NewsService
 *
 * @package OctoCmsModule\Blog\Services
 */
class NewsService implements NewsServiceInterface
{
    /**
     * @param News  $news
     * @param array $fields
     *
     * @return News
     */
    public function saveNews(News $news, array $fields): News
    {
        $news->fill($fields);

        $news->date = Carbon::createFromFormat(
            'Y-m-d',
            Arr::get($fields, 'date', '')
        )->format('Y-m-d');

        $news->save();

        $this->saveNewsLangs($news, Arr::get($fields, 'newsLangs', []));

        $this->saveNewsCategories($news, Arr::get($fields, 'categories', []));

        $news->retag(Arr::get($fields, 'tags', []));

        return $news;
    }

    /**
     * @param News  $news
     * @param array $newsLangs
     */
    protected function saveNewsLangs(News $news, array $newsLangs)
    {
        foreach ($newsLangs as $newsLang) {
            NewsLang::updateOrCreate(
                [
                    'news_id' => $news->id,
                    'lang'    => Arr::get($newsLang, 'lang', ''),
                ],
                [
                    'title'             => Arr::get($newsLang, 'title', ''),
                    'short_description' => Arr::get($newsLang, 'short_description', ''),
                ]
            );
        }
    }

    /**
     * @param News $news
     *
     * @return Page
     */
    public function storeNewsPage(News $news): Page
    {

        $moduleSettings = SettingService::getSettingByName("module_" . strtolower(config('blog.name')));

        $pageName = LanguageUtils::getLangValue($news->newsLangs, 'title');

        /** @var Page $page */
        $page = new Page([
            'name'      => !empty($pageName) ? $pageName : (Page::TYPE_NEWS . '-' . $news->id),
            'type'      => Page::TYPE_NEWS,
            'published' => Arr::get($moduleSettings, "news.page_publish", true),
        ]);

        $page->pageable()->associate($news)->save();

        $baseUrls = Arr::get($moduleSettings, "news.base_url", []);

        foreach ($news->newsLangs as $newsLang) {
            $filtered = Arr::first($baseUrls, function ($item) use ($newsLang) {
                return $item['lang'] == $newsLang->lang;
            });

            $pageLang = new PageLang([
                'url'              => (!empty($filtered) ? ($filtered['value'] . "/") : "") .
                    Str::slug($newsLang->title, '-'),
                'lang'             => $newsLang->lang,
                'meta_title'       => $newsLang->title,
                'meta_description' => $newsLang->short_description,
            ]);

            $pageLang->page()->associate($page)->save();
        }

        return $page;
    }

    /**
     * @param News  $news
     * @param array $categories
     */
    protected function saveNewsCategories(News $news, array $categories)
    {
        /** @var array $categoriesId */
        $categoriesId = [];

        foreach ($categories as $category) {
            $categoriesId[Arr::get($category, 'id', null)] = [
                'main' => Arr::get($category, 'main', false),
            ];
        }

        $news->categories()->sync($categoriesId);
    }
}
