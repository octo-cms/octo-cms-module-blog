<?php

namespace OctoCmsModule\Blog\Services;

use OctoCmsModule\Core\Services\PictureService as PictureServiceCore;
use OctoCmsModule\Blog\Interfaces\PictureServiceInterface;

/**
 * Class PictureService
 *
 * @package OctoCmsModule\Blog\Services
 */
class PictureService extends PictureServiceCore implements PictureServiceInterface
{

}
