<?php

namespace OctoCmsModule\Blog\Tests\Mocks\Services;

use OctoCmsModule\Core\Tests\Mocks\PictureServiceMock as PictureServiceMockCore;
use OctoCmsModule\Blog\Interfaces\PictureServiceInterface;

/**
 * Class PictureServiceMock
 *
 * @package OctoCmsModule\Blog\Tests\Mocks\Services
 */
class PictureServiceMock extends PictureServiceMockCore implements PictureServiceInterface
{

}
