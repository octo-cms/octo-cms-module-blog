<?php

namespace OctoCmsModule\Blog\Tests\Mocks\Services;

use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Interfaces\NewsContentServiceInterface;

/**
 * Class NewsContentServiceMock
 *
 * @package OctoCmsModule\Blog\Tests\Mocks\Services
 */
class NewsContentServiceMock implements NewsContentServiceInterface
{
    /**
     * @param News  $news
     * @param array $fields
     *
     * @return News
     */
    public function updateNewsContents(News $news, array $fields): News
    {
        return $news;
    }
}
