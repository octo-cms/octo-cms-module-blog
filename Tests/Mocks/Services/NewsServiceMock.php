<?php

namespace OctoCmsModule\Blog\Tests\Mocks\Services;

use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Interfaces\NewsServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Page;

class NewsServiceMock implements NewsServiceInterface
{
    /**
     * @param News  $service
     * @param array $fields
     *
     * @return News
     */
    public function saveNews(News $service, array $fields): News
    {
        return News::factory()->create();
    }

    /**
     * @param News $news
     *
     * @return Page
     */
    public function storeNewsPage(News $news): Page
    {
        return Page::factory()->create([
            'type'          => Page::TYPE_NEWS,
            'pageable_type' => Page::PAGEABLE_TYPE_NEWS,
            'pageable_id'   => $news->id,
        ]);
    }
}
