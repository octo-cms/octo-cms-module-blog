<?php

namespace OctoCmsModule\Blog\Tests\Mocks\Services;

use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Interfaces\CategoryServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class CategoryService
 *
 * @package OctoCmsModule\Blog\Services
 */
class CategoryServiceMock implements CategoryServiceInterface
{
    /**
     * @param Category $category
     * @param array    $fields
     *
     * @return Category
     */
    public function saveCategory(Category $category, array $fields): Category
    {
        return $category;
    }

    /**
     * @param Category $category
     *
     * @return Page
     */
    public function storeCategoryPage(Category $category): Page
    {
        return Page::factory()->create([
            'type'          => Page::TYPE_CATEGORY_NEWS,
            'pageable_type' => Page::PAGEABLE_TYPE_CATEGORY_NEWS,
            'pageable_id'   => $category->id,
        ]);
    }

    /**
     * @param Builder $categories
     * @param array   $filters
     * @param string  $query
     *
     * @return Builder
     */
    public function filterCategoryBuilder(Builder $categories, array $filters = [], string $query = ''): Builder
    {
        return $categories;
    }
}
