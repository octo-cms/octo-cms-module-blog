<?php

namespace OctoCmsModule\Blog\Tests\Commands;

use Faker\Generator as Faker;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Console\FakeBlogCommand;
use OctoCmsModule\Blog\Console\InstallBlogCommand;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use ReflectionException;

/**
 * Class FakeBlogCommandTest
 *
 * @package OctoCmsModule\Blog\Tests\Commands
 */
class FakeBlogCommandTest extends TestCase
{


    public function test_installCatalog()
    {
        $this->artisan('fake:blog')
            ->expectsOutput('Running Fake Blog Command ...')
            ->expectsOutput('Creating Categories ...')
            ->expectsOutput('Creating News ...');
    }

    /**
     * @throws ReflectionException
     */
    public function test_createCategories()
    {
        /** @var InstallBlogCommand */
        $fakeBlogCommand = new FakeBlogCommand(new SettingServiceMock(), new Faker());

        /** @var array $langs */
        $langs = ['it', 'en'];

        $this->invokeMethod(
            $fakeBlogCommand,
            'createCategories',
            [$langs]
        );

        $this->assertDatabaseHas('pages', [
            'type' => Page::TYPE_CATEGORY_NEWS,
        ]);

        foreach ($langs as $lang) {
            $this->assertDatabaseHas('page_langs', [
                'page_id' => 1,
                'lang'    => $lang,
            ]);
        }

    }

    /**
     * @throws ReflectionException
     */
    public function test_createNews()
    {
        $category = Category::factory()->create();

        /** @var FakeBlogCommand $fakeBlogCommand */
        $fakeBlogCommand = new FakeBlogCommand(new SettingServiceMock(), new Faker());

        /** @var array $langs */
        $langs = ['it', 'en'];

        $this->invokeMethod(
            $fakeBlogCommand,
            'createNews',
            [$langs]
        );

        $this->assertDatabaseHas('pages', [
            'type' => Page::TYPE_NEWS,
        ]);

        foreach ($langs as $lang) {
            $this->assertDatabaseHas('page_langs', [
                'page_id' => 1,
                'lang'    => $lang,
            ]);
        }

        for ($newsId = 1; $newsId <= 5; $newsId++) {
            $this->assertDatabaseHas('blog_category_news', [
                'category_id' => $category->id,
                'news_id'     => $newsId,
            ]);
        }
    }
}
