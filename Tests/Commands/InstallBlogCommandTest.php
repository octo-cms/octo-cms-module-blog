<?php

namespace OctoCmsModule\Blog\Tests\Commands;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Console\InstallBlogCommand;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use ReflectionException;

/**
 * Class InstallCatalogCommandTest
 *
 * @package OctoCmsModule\Catalog\Tests\Commands
 */
class InstallBlogCommandTest extends TestCase
{



    /** @var string[] */
    public $langs = ['it', 'en'];

    public function test_installCatalog()
    {
        $this->artisan('install:blog')
            ->expectsOutput('Running Install Blog Command ...')
            ->expectsOutput('Creating Blog Page ...');
    }

    /**
     * @throws ReflectionException
     */
    public function test_createBlogPage()
    {
        /** @var InstallBlogCommand */
        $installBlogCommand = new InstallBlogCommand(new SettingServiceMock());

        $this->invokeMethod(
            $installBlogCommand,
            'createBlogPage',
            [$this->langs]
        );

        $this->assertDatabaseHas('pages', [
            'name' => 'Blog',
            'type' => Page::TYPE_BLOG,
        ]);

        foreach ($this->langs as $lang) {
            $this->assertDatabaseHas('page_langs', [
                'page_id' => 1,
                'lang'    => $lang,
                'url'     => 'blog',
            ]);
        }
    }

    /**
     * @throws ReflectionException
     */
    public function test_createSettings()
    {
        /** @var InstallBlogCommand */
        $installBlogCommand = new InstallBlogCommand(new SettingServiceMock());

        $this->invokeMethod(
            $installBlogCommand,
            'createSettings',
            [$this->langs]
        );

        $this->assertDatabaseHas('settings', [
            'name'  => 'module_blog',
            'value' => serialize([
                'news'       => [
                    'page_publish' => true,
                    'pictures'     => [
                        'main' => [
                            'image_width'  => 500,
                            'image_height' => 500,
                        ],
                    ],
                    'base_url'     => [
                        ['value' => 'news', 'lang' => 'it'],
                        ['value' => 'news', 'lang' => 'en'],
                    ],
                ],
                'categories' => [
                    'page_publish' => true,
                    'pictures'     => [
                        'main' => [
                            'image_width'  => 500,
                            'image_height' => 500,
                        ],
                    ],
                    'base_url'     => [
                        ['value' => 'categories', 'lang' => 'it'],
                        ['value' => 'categories', 'lang' => 'en'],
                    ],
                ],
            ]),
        ]);
    }
}
