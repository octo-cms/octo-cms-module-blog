<?php

namespace OctoCmsModule\Blog\Tests\Entities;

use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsLangTest
 *
 * @package OctoCmsModule\Blog\Tests\Entities
 */
class NewsLangTest extends TestCase
{


    public function test_NewsLangBelongsToNews()
    {
        /** @var NewsLang $newsLang */
        $newsLang = NewsLang::factory()->create();

        $newsLang->load('news');

        $this->assertInstanceOf(News::class, $newsLang->news);
    }
}
