<?php

namespace OctoCmsModule\Blog\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CategoryTest
 *
 * @package OctoCmsModule\Blog\Tests\Entities
 */
class CategoryTest extends TestCase
{


    public function test_CategoryHasManyCategoryLangs()
    {
        /** @var Category $category */
        $category = Category::factory()->has(CategoryLang::factory()->count(2))->create();

        $category->load('categoryLangs');

        $this->assertInstanceOf(Collection::class, $category->categoryLangs);
        $this->assertInstanceOf(CategoryLang::class, $category->categoryLangs->first());
    }

    public function test_ParentChild()
    {
        /** @var Category $parent */
        $parent = Category::factory()->create();

        /** @var Category $child */
        $child = Category::factory()->create(['parent_id' => $parent->id]);

        $parent->load('children');

        $this->assertInstanceOf(Collection::class, $parent->children);
        $this->assertInstanceOf(Category::class, $parent->children->first());

        $this->assertEquals(
            $child->id,
            $parent->children->first()->id
        );

        $child->load('parent');

        $this->assertInstanceOf(Category::class, $child->parent);

        $this->assertEquals(
            $parent->id,
            $child->parent->id
        );
    }

    public function test_Tree()
    {
        /** @var Category $parent1 */
        $parent1 = Category::factory()
            ->has(Category::factory()->count(2), 'children')
            ->create();

        /** @var Category $child */
        $child = Category::factory()->create([
            'parent_id' => $parent1->id,
        ]);

        Category::factory()->create([
            'parent_id' => $child->id,
        ]);

        Category::factory()
            ->has(Category::factory()->count(2), 'children')
            ->create();

        Category::factory()
            ->has(Category::factory()->count(2), 'children')
            ->create();

        $tree = Category::getTree()->toArray();

        $this->assertCount(3, $tree);

        $this->assertCount(3, $tree[0]['children']);
        $this->assertCount(2, $tree[1]['children']);
        $this->assertCount(2, $tree[2]['children']);

        $this->assertCount(1, $tree[0]['children'][2]['children']);

    }

    public function test_TreeWithLang()
    {
        /** @var Category $parent */
        $parent = Category::factory()->has(CategoryLang::factory()->count(2))->create();

        /** @var Category $child */
        $child = Category::factory()
            ->has(CategoryLang::factory()->count(2))
            ->create(['parent_id' => $parent->id]);

        Category::factory()
            ->has(CategoryLang::factory()->count(2))
            ->create(['parent_id' => $child->id]);

        $tree = Category::getTree()->toArray();

        $this->assertArrayHasKey('children', $tree[0]);
        $this->assertArrayHasKey('category_langs', $tree[0]);

        $this->assertArrayHasKey('children', $tree[0]['children'][0]);
        $this->assertArrayHasKey('category_langs', $tree[0]['children'][0]);

        $this->assertArrayHasKey('category_langs', $tree[0]['children'][0]['children'][0]);

    }

    public function test_parents()
    {
        /** @var Category $firstCategory */
        $firstCategory = Category::factory()->create();

        /** @var Category $secondCategory */
        $secondCategory = Category::factory()->create([
            'parent_id' => $firstCategory->id,
        ]);

        /** @var Category $thirdCategory */
        $thirdCategory = Category::factory()->create([
            'parent_id' => $secondCategory->id,
        ]);

        /** @var Category $fourthCategory */
        $fourthCategory = Category::factory()->create([
            'parent_id' => $thirdCategory->id,
        ]);

        $fourthCategory->load('parents');

        $this->assertInstanceOf(Category::class, $fourthCategory->parents);

        $this->assertEquals(
            $thirdCategory->id,
            $fourthCategory->parents->id
        );

        $this->assertEquals(
            $secondCategory->id,
            $fourthCategory->parents->parents->id
        );

        $this->assertEquals(
            $firstCategory->id,
            $fourthCategory->parents->parents->parents->id
        );
    }

    public function test_PageHasOneCategory()
    {
        /** @var Page $page */
        $page = Page::factory()->create();

        /** @var Category $category */
        $category = Category::factory()->create();

        $page->pageable()->associate($category)->save();

        $page->load('pageable');

        $this->assertEquals(
            Page::PAGEABLE_TYPE_CATEGORY_NEWS,
            $page->pageable_type
        );

        $this->assertInstanceOf(Category::class, $page->pageable);
    }
}
