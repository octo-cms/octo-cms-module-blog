<?php

namespace OctoCmsModule\Blog\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsTest
 *
 * @package OctoCmsModule\Blog\Tests\Entities
 */
class NewsTest extends TestCase
{


    public function test_NewsHasManyNewsLangs()
    {
        /** @var News $news */
        $news = News::factory()->has(NewsLang::factory()->count(3))->create();

        $news->load('newsLangs');

        $this->assertInstanceOf(Collection::class, $news->newsLangs);
        $this->assertInstanceOf(NewsLang::class, $news->newsLangs->first());
    }

    public function test_PageHasOneNews()
    {
        /** @var Page $page */
        $page = Page::factory()->create();

        $page->pageable()->associate(News::factory()->create());

        $page->load('pageable');

        $this->assertEquals(
            Page::PAGEABLE_TYPE_NEWS,
            $page->pageable_type
        );

        $this->assertInstanceOf(News::class, $page->pageable);
    }

    public function test_NewsHasManyPictures()
    {
        /** @var News $news */
        $news = News::factory()->has(Picture::factory()->count(2))->create();

        $news->load('pictures');

        $this->assertInstanceOf(Collection::class, $news->pictures);
        $this->assertInstanceOf(Picture::class, $news->pictures->first());
    }

    public function test_NewsHasManyPicturesWithMainTag()
    {
        /** @var News $news */
        $news = News::factory()
            ->has(
                Picture::factory()->count(2)->state([
                    'tag' => Picture::TAG_MAIN,
                ])
            )->has(
                Picture::factory()->count(3)->state([
                    'tag' => 'not main',
                ])
            )->create();

        /** @var Collection $pictures */
        $pictures = $news->getPictures(Picture::TAG_MAIN)->get();

        $this->assertInstanceOf(Collection::class, $pictures);

        $this->assertInstanceOf(Picture::class, $pictures->first());

        $this->assertEquals(Picture::TAG_MAIN, $pictures->first()->tag);

        $this->assertCount(2, $pictures);
    }

    public function test_NewsLoadPicturesWithMainTag()
    {
        /** @var News $news */
        $news = News::factory()
            ->has(
                Picture::factory()->count(2)->state([
                    'tag' => Picture::TAG_MAIN,
                ])
            )->has(
                Picture::factory()->count(3)->state([
                    'tag' => 'not main',
                ])
            )->create();

        $news->loadPictures(Picture::TAG_MAIN);

        $this->assertInstanceOf(Collection::class, $news->pictures);

        $this->assertCount(2, $news->pictures);

        $this->assertInstanceOf(Picture::class, $news->pictures->first());

        $this->assertEquals(Picture::TAG_MAIN, $news->pictures->first()->tag);
    }

    public function test_NewsBelongsToManyRelatedNews()
    {
        /** @var News $mainNews */
        $mainNews = News::factory()->has(News::factory()->count(3), 'relateds')->create();

        $mainNews->load('relateds');

        $this->assertCount(3, $mainNews->relateds);
        $this->assertInstanceOf(Collection::class, $mainNews->relateds);
        $this->assertInstanceOf(News::class, $mainNews->relateds->first());
    }

    public function test_NewsHasManyNewsContents()
    {
        /** @var News $news */
        $news = News::factory()->has(NewsContent::factory()->count(3))->create();

        $news->load('newsContents');

        $this->assertInstanceOf(Collection::class, $news->newsContents);
        $this->assertInstanceOf(NewsContent::class, $news->newsContents->first());
    }

    public function test_NewsHasManyNewsContentsSorted()
    {
        /** @var News $news */
        $news = News::factory()->create();

        for ($i = 3; $i > 0; $i--) {
            NewsContent::factory()->create([
                'news_id'  => $news->id,
                'position' => $i,
            ]);
        }

        $news->load('newsContents');

        $this->assertInstanceOf(Collection::class, $news->newsContents);
        $this->assertCount(3, $news->newsContents);
        $plucked = $news->newsContents->pluck('position')->all();
        $this->assertEquals(['1', '2', '3'], $plucked);
    }
}
