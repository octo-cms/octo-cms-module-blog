<?php

namespace OctoCmsModule\Blog\Tests\Entities;

use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Entities\NewsContentLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsContentLangTest
 *
 * @package OctoCmsModule\Blog\Tests\Entities
 */
class NewsContentLangTest extends TestCase
{


    public function test_NewsContentLangBelongsToNewsContent()
    {
        /** @var NewsContentLang $newsContentLang */
        $newsContentLang = NewsContentLang::factory()->create();

        $newsContentLang->load('newsContent');

        $this->assertInstanceOf(NewsContent::class, $newsContentLang->newsContent);
    }
}
