<?php

namespace OctoCmsModule\Blog\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Entities\NewsContentLang;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\Video;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsContentTest
 *
 * @package OctoCmsModule\Blog\Tests\Entities
 */
class NewsContentTest extends TestCase
{


    public function test_NewsContentHasManyNewsContentLangs()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()->has(NewsContentLang::factory()->count(2))->create();

        $newsContent->load('newsContentLangs');

        $this->assertInstanceOf(Collection::class, $newsContent->newsContentLangs);
        $this->assertInstanceOf(NewsContentLang::class, $newsContent->newsContentLangs->first());
    }

    public function test_NewsContentBelongsToNews()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()->create();

        $newsContent->load('news');

        $this->assertInstanceOf(News::class, $newsContent->news);
    }

    public function test_NewsContentHasManyPictures()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()->has(Picture::factory()->count(2))->create();

        $newsContent->load('pictures');

        $this->assertInstanceOf(Collection::class, $newsContent->pictures);
        $this->assertInstanceOf(Picture::class, $newsContent->pictures->first());
    }

    public function test_NewsContentHasManyPicturesWithMainTag()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()
            ->has(
                Picture::factory()->count(2)->state([
                    'tag' => Picture::TAG_MAIN,
                ])
            )->has(
                Picture::factory()->count(3)->state([
                    'tag' => 'not main',
                ])
            )->create();

        /** @var Collection $pictures */
        $pictures = $newsContent->getPictures(Picture::TAG_MAIN)->get();

        $this->assertInstanceOf(Collection::class, $pictures);

        $this->assertInstanceOf(Picture::class, $pictures->first());

        $this->assertEquals(Picture::TAG_MAIN, $pictures->first()->tag);

        $this->assertCount(2, $pictures);
    }

    public function test_NewsContentLoadPicturesWithMainTag()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()
            ->has(
                Picture::factory()->count(2)->state([
                    'tag' => Picture::TAG_MAIN,
                ])
            )->has(
                Picture::factory()->count(3)->state([
                    'tag' => 'not main',
                ])
            )->create();

        $newsContent->loadPictures(Picture::TAG_MAIN);

        $this->assertInstanceOf(Collection::class, $newsContent->pictures);

        $this->assertCount(2, $newsContent->pictures);

        $this->assertInstanceOf(Picture::class, $newsContent->pictures->first());

        $this->assertEquals(Picture::TAG_MAIN, $newsContent->pictures->first()->tag);
    }

    public function test_NewsContentHasManyVideos()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()->has(Video::factory()->count(2))->create();

        $newsContent->load('videos');

        $this->assertInstanceOf(Collection::class, $newsContent->videos);
        $this->assertInstanceOf(Video::class, $newsContent->videos->first());
    }

    public function test_VideoHasOneNewsContent()
    {
        /** @var Video $video */
        $video = Video::factory()->create();

        $video->videable()->associate(
            NewsContent::factory()->create()
        )->save();

        $video->load('videable');

        $this->assertEquals(
            Video::VIDEABLE_TYPE_NEWS_CONTENT,
            $video->videable_type
        );

        $this->assertInstanceOf(NewsContent::class, $video->videable);
    }
}
