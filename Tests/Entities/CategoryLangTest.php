<?php

namespace OctoCmsModule\Blog\Tests\Entities;

use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CategoryLangTest
 *
 * @package OctoCmsModule\Blog\Tests\Entitieswe
 */
class CategoryLangTest extends TestCase
{


    public function test_CategoryLangBelongsToCategory()
    {
        /** @var CategoryLang $categoryLang */
        $categoryLang = CategoryLang::factory()->create();

        $categoryLang->load('category');

        $this->assertInstanceOf(Category::class, $categoryLang->category);
    }
}
