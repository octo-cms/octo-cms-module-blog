<?php

namespace OctoCmsModule\Blog\Tests\Services\NewsService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Blog\Services\NewsService;

/**
 * Class SaveNewsTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\NewsService
 */
class SaveNewsTest extends TestCase
{



    public function test_storeNews()
    {
        /** @var array $tags */
        $tags = ['tag 1', 'tag 2'];

        /** @var array $fields */
        $fields = [
            'active'    => true,
            'author'    => 'author',
            'date'      => '1974-01-01',
            'newsLangs' => [
                [
                    'lang'              => 'it',
                    'title'             => 'titolo',
                    'short_description' => 'descrizione breve',
                ],
            ],
            'tags'      => $tags,
        ];

        /** @var NewsService $newsService */
        $newsService = new NewsService();

        $newsService->saveNews(new News(), $fields);

        $this->assertDatabaseHas('blog_news', [
            'id'     => 1,
            'active' => $fields['active'],
            'author' => $fields['author'],
        ]);

        foreach ($fields['newsLangs'] as $newsLang) {
            $this->assertDatabaseHas('blog_news_langs',
                [
                    'news_id'           => 1,
                    'lang'              => $newsLang['lang'],
                    'title'             => $newsLang['title'],
                    'short_description' => $newsLang['short_description'],
                ]
            );
        }

        foreach ($tags as $tag) {
            $this->assertDatabaseHas('tagging_tagged', [
                'taggable_id'   => 1,
                'taggable_type' => 'News',
                'tag_name'      => ucwords($tag),
            ]);
        }
    }

    public function test_updateNews()
    {
        /** @var array $updatedTags */
        $updatedTags = ['tag 1 updated', 'tag 2 updated'];

        /** @var array $fields */
        $fields = [
            'active'    => false,
            'author'    => 'new author',
            'date'      => '1974-01-01',
            'newsLangs' => [
                [
                    'lang'              => 'it',
                    'title'             => 'nuovo titolo',
                    'short_description' => 'nuova descrizione breve',
                ],
                [
                    'lang'              => 'en',
                    'title'             => 'new title',
                    'short_description' => 'new short description',
                ],
            ],
            'tags'      => $updatedTags,
        ];

        /** @var News $news */
        $news = News::factory()
            ->has(NewsLang::factory()->state([
                'lang'              => 'it',
                'title'             => 'titolo',
                'short_description' => 'descrizione breve',
            ]))
            ->has(NewsLang::factory()->state([
                'lang'              => 'en',
                'title'             => 'title',
                'short_description' => 'short description',
            ]))
            ->create();

        $news->tag(['tag 1', 'tag 2']);

        /** @var NewsService $newsService */
        $newsService = new NewsService();

        $newsService->saveNews($news, $fields);

        $this->assertDatabaseHas('blog_news', [
            'id'     => $news->id,
            'active' => $fields['active'],
            'author' => $fields['author'],
        ]);

        foreach ($fields['newsLangs'] as $newsLang) {
            $this->assertDatabaseHas('blog_news_langs',
                [
                    'news_id'           => $news->id,
                    'lang'              => $newsLang['lang'],
                    'title'             => $newsLang['title'],
                    'short_description' => $newsLang['short_description'],
                ]
            );
        }

        foreach ($updatedTags as $tag) {
            $this->assertDatabaseHas('tagging_tagged', [
                'taggable_id'   => $news->id,
                'taggable_type' => 'News',
                'tag_name'      => ucwords($tag),
            ]);
        }
    }
}
