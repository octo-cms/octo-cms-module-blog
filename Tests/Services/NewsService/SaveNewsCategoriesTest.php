<?php

namespace OctoCmsModule\Blog\Tests\Services\NewsService;

use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Services\NewsService;

/**
 * Class SaveNewsCategoriesTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\NewsService
 */
class SaveNewsCategoriesTest extends TestCase
{


    /**
     * @throws \ReflectionException
     */
    public function test_saveCategories()
    {
        /** @var News $news */
        $news = News::factory()->create();

        /** @var Category $formerMainCategory */
        $formerMainCategory = Category::factory()->create();

        /** @var Category $newMainCategory */
        $newMainCategory = Category::factory()->create();

        /** @var Category $categoryToBeDetached */
        $categoryToBeDetached = Category::factory()->create();

        $news->categories()->attach([
            $formerMainCategory->id   => ['main' => true],
            $newMainCategory->id      => ['main' => false],
            $categoryToBeDetached->id => ['main' => false],
        ]);

        $categories = [
            [
                'id'   => $formerMainCategory->id,
                'main' => false
            ],
            [
                'id'   => $newMainCategory->id,
                'main' => true
            ],
        ];

        /** @var NewsService $newsService */
        $newsService = new NewsService();

        $this->invokeMethod(
            $newsService,
            'saveNewsCategories',
            [$news, $categories]
        );

        $this->assertDatabaseHas('blog_category_news', [
            'category_id' => $formerMainCategory->id,
            'main'        => false,
        ]);

        $this->assertDatabaseHas('blog_category_news', [
            'category_id' => $newMainCategory->id,
            'main'        => true,
        ]);

        $this->assertDatabaseMissing('blog_category_news', [
            'category_id' => $categoryToBeDetached->id
        ]);
    }
}
