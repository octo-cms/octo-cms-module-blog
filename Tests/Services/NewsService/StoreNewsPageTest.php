<?php

namespace OctoCmsModule\Blog\Tests\Services\NewsService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Blog\Services\NewsService;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Entities\Setting;

/**
 * Class StoreNewsPageTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\NewsService
 */
class StoreNewsPageTest extends TestCase
{


    public function test_storePage()
    {
        /** @var News $news */
        $news = News::factory()->create();

        /** @var NewsLang $itNews */
        $itNews = NewsLang::factory()->create([
            'news_id' => $news->id,
            'lang'    => 'it',
            'title'   => 'titolo della notizia',
        ]);

        /** @var NewsLang $enNews */
        $enNews = NewsLang::factory()->create([
            'news_id' => $news->id,
            'lang'    => 'en',
            'title'   => 'news title',
        ]);

        /** @var NewsService $newsService */
        $newsService = new NewsService();

        /** @var Page $newsPage */
        $newsPage = $newsService->storeNewsPage($news);

        $this->assertDatabaseHas('pages', [
            'id'   => 1,
            'name' => 'titolo della notizia',
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'              => 'titolo-della-notizia',
            'page_id'          => $newsPage->id,
            'lang'             => $itNews->lang,
            'meta_title'       => $itNews->title,
            'meta_description' => $itNews->short_description,
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'              => 'news-title',
            'page_id'          => $newsPage->id,
            'lang'             => $enNews->lang,
            'meta_title'       => $enNews->title,
            'meta_description' => $enNews->short_description,
        ]);
    }

    public function test_createNewsWithPublishFalse()
    {
        /** @var News $news */
        $news = News::factory()
            ->has(NewsLang::factory()->state([
                'lang'  => 'it',
                'title' => 'titolo della notizia unpublished',
            ]))
            ->create();

        $data = [
            'news' => [
                'page_publish' => false,
                'image_width'  => 500,
                'image_height' => 500,
                'base_url'     => [],
            ],
        ];

        Setting::updateOrCreate(
            ['name' => 'module_' . strtolower(config('blog.name'))],
            ['value' => $data]
        );

        /** @var NewsService $newsService */
        $newsService = new NewsService();

        $newsService->storeNewsPage($news);

        $this->assertDatabaseHas('pages', [
            'name'      => 'titolo della notizia unpublished',
            'published' => false,
        ]);

    }

    public function test_createNewsLangPrefix()
    {

        /** @var News $news */
        $news = News::factory()
            ->has(NewsLang::factory()->state([
                'lang'  => 'it',
                'title' => 'titoloconprefisso',
            ]))
            ->create();

        $data = [
            'news' => [
                'page_publish' => true,
                'image_width'  => 500,
                'image_height' => 500,
                'base_url'     => [
                    [
                        'value' => 'notizie',
                        'lang'  => 'it',
                    ],
                ],
            ],
        ];

        Setting::updateOrCreate(
            ['name' => 'module_' . strtolower(config('blog.name'))],
            ['value' => $data]
        );

        /** @var NewsService $newsService */
        $newsService = new NewsService();

        $newsService->storeNewsPage($news);

        $this->assertDatabaseHas('page_langs', [
            'url'  => 'notizie/titoloconprefisso',
            'lang' => 'it',
        ]);

    }


    public function test_createNewsLangPrefixTwoLangs()
    {
        /** @var News $news */
        $news = News::factory()
            ->has(NewsLang::factory()->state([
                'lang'  => 'it',
                'title' => 'titoloconprefisso',
            ]))
            ->has(NewsLang::factory()->state([
                'lang'  => 'en',
                'title' => 'titlewithprefix',
            ]))
            ->create();

        $data = [
            'news' => [
                'base_url' => [
                    [
                        'value' => 'notizie',
                        'lang'  => 'it',
                    ],
                    [
                        'value' => 'news',
                        'lang'  => 'en',
                    ],
                ],
            ],
        ];

        Setting::updateOrCreate(
            ['name' => 'module_' . strtolower(config('blog.name'))],
            ['value' => $data]
        );

        (new NewsService())->storeNewsPage($news);

        $this->assertDatabaseHas('page_langs', [
            'url'  => 'notizie/titoloconprefisso',
            'lang' => 'it',
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'  => 'news/titlewithprefix',
            'lang' => 'en',
        ]);
    }
}
