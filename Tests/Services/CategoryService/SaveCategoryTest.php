<?php

namespace OctoCmsModule\Blog\Tests\Services\CategoryService;

use Illuminate\Support\Str;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Blog\Services\CategoryService;

/**
 * Class SaveCategoryTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\CategoryService
 */
class SaveCategoryTest extends TestCase
{


    public function test_storeParentCategory()
    {
        /** @var array $tags */
        $tags = ['tag 1', 'tag 2'];

        $fields = [
            'parent_id'     => null,
            'active'        => true,
            'categoryLangs' => [
                [
                    'lang'              => 'it',
                    'name'              => 'nome',
                    'description'       => 'descrizione',
                    'short_description' => 'descrizione breve',
                ],
            ],
            'tags'          => $tags,
        ];

        /** @var CategoryService $categoryService */
        $categoryService = new CategoryService();

        $categoryService->saveCategory(new Category(), $fields);

        $this->assertDatabaseHas('blog_categories', [
            'id'        => 1,
            'parent_id' => null,
        ]);

        foreach ($fields['categoryLangs'] as $categoryLang) {
            $this->assertDatabaseHas('blog_category_langs',
                [
                    'category_id'       => 1,
                    'lang'              => $categoryLang['lang'],
                    'name'              => $categoryLang['name'],
                    'slug'              => Str::slug($categoryLang['name']),
                    'description'       => $categoryLang['description'],
                    'short_description' => $categoryLang['short_description'],
                ]
            );
        }

        foreach ($tags as $tag) {
            $this->assertDatabaseHas('tagging_tagged', [
                'taggable_id'   => 1,
                'taggable_type' => 'CategoryNews',
                'tag_name'      => ucwords($tag),
            ]);
        }
    }

    public function test_storeChildCategory()
    {
        /** @var Category $parentCategory */
        $parentCategory = Category::factory()->create();

        $fields = [
            'parent_id'     => $parentCategory->id,
            'categoryLangs' => [
                [
                    'lang'              => 'it',
                    'name'              => 'nome',
                    'description'       => 'descrizione',
                    'short_description' => 'descrizione breve',
                ],
            ],
        ];

        /** @var CategoryService $categoryService */
        $categoryService = new CategoryService();

        /** @var Category $category */
        $category = $categoryService->saveCategory(new Category(), $fields);

        $this->assertDatabaseHas('blog_categories', [
            'id'        => $category->id,
            'parent_id' => $parentCategory->id,
        ]);

        foreach ($fields['categoryLangs'] as $categoryLang) {
            $this->assertDatabaseHas('blog_category_langs',
                [
                    'category_id'       => $category->id,
                    'lang'              => $categoryLang['lang'],
                    'name'              => $categoryLang['name'],
                    'slug'              => Str::slug($categoryLang['name']),
                    'description'       => $categoryLang['description'],
                    'short_description' => $categoryLang['short_description'],
                ]
            );
        }
    }

    public function test_updateCategory()
    {
        /** @var array $updatedTags */
        $updatedTags = ['tag 1 updated', 'tag 2 updated'];

        /** @var array $fields */
        $fields = [
            'parent_id'     => null,
            'categoryLangs' => [
                [
                    'lang'              => 'it',
                    'name'              => 'nome aggiornato',
                    'description'       => 'descrizione aggiornata',
                    'short_description' => 'descrizione breve aggiornata',
                ],
                [
                    'lang'              => 'en',
                    'name'              => 'updated name',
                    'description'       => 'updated description',
                    'short_description' => 'updated short description',
                ],
            ],
            'tags'          => $updatedTags,
        ];

        /** @var Category $category */
        $category = Category::factory()
            ->has(CategoryLang::factory()->state([
                'lang'              => 'it',
                'name'              => 'nome',
                'description'       => 'descrizione',
                'short_description' => 'descrizione breve',
            ]))
            ->has(CategoryLang::factory()->state([
                'lang'              => 'it',
                'name'              => 'name',
                'description'       => 'description',
                'short_description' => 'short description',
            ]))
            ->create();

        $category->tag(['tag 1', 'tag 2']);

        /** @var CategoryService $categoryService */
        $categoryService = new CategoryService();

        $categoryService->saveCategory($category, $fields);

        $this->assertDatabaseHas('blog_categories', [
            'id'        => $category->id,
            'parent_id' => null,
        ]);

        foreach ($fields['categoryLangs'] as $categoryLang) {
            $this->assertDatabaseHas('blog_category_langs',
                [
                    'category_id'       => $category->id,
                    'lang'              => $categoryLang['lang'],
                    'name'              => $categoryLang['name'],
                    'slug'              => Str::slug($categoryLang['name']),
                    'description'       => $categoryLang['description'],
                    'short_description' => $categoryLang['short_description'],
                ]
            );
        }

        foreach ($updatedTags as $tag) {
            $this->assertDatabaseHas('tagging_tagged', [
                'taggable_id'   => $category->id,
                'taggable_type' => 'CategoryNews',
                'tag_name'      => ucwords($tag),
            ]);
        }
    }
}
