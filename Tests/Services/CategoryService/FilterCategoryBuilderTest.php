<?php

namespace OctoCmsModule\Blog\Tests\Services\CategoryService;

use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Blog\Services\CategoryService;

/**
 * Class FilterCategoryBuilderTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\CategoryService
 */
class FilterCategoryBuilderTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $filters = [
            'active'    => false,
            'parent_id' => 1,
        ];

        $providers[] = [$filters];

        $fields = $filters;
        $fields['active'] = true;
        $providers[] = [$fields];

        $fields = $filters;
        unset($fields['parent_id']);
        $providers[] = [$fields];

        return $providers;
    }

    /**
     * @param $filters
     *
     * @dataProvider dataProvider
     */
    public function test_filters($filters)
    {
        Category::factory()
            ->has(Category::factory()->state(['active' => $filters['active']]), 'children')
            ->create();

        /** @var CategoryService $categoryService */
        $categoryService = new CategoryService();

        /** @var Builder $categoryBuilderFromService */
        $categoryBuilderFromService = $categoryService->filterCategoryBuilder(
            Category::query(),
            $filters,
            ''
        );

        $this->assertInstanceOf(Builder::class, $categoryBuilderFromService);

        $this->assertCount(1, $categoryBuilderFromService->get());
    }

    public function test_querySearchCategoryLang()
    {
        /** @var string $query */
        $query = 'category lang name';

        /** @var Category $category */
        $category = Category::factory()
            ->has(CategoryLang::factory()->state(['name' => $query]))
            ->create();

        /** @var CategoryService $categoryService */
        $categoryService = new CategoryService();

        /** @var Builder $categoryBuilderFromService */
        $categoryBuilderFromService = $categoryService->filterCategoryBuilder(
            Category::query(),
            [],
            $query
        );

        $this->assertInstanceOf(Builder::class, $categoryBuilderFromService);

        $this->assertCount(1, $categoryBuilderFromService->get());

        $this->assertEquals(
            $category->id,
            $categoryBuilderFromService->first()->id
        );
    }
}
