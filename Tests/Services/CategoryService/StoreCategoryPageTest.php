<?php

namespace OctoCmsModule\Blog\Tests\Services\CategoryService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Blog\Services\CategoryService;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class StoreCategoryPageTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\CategoryService
 */
class StoreCategoryPageTest extends TestCase
{



    public function test_storeCategoryPage()
    {
        /** @var Category $category */
        $category = Category::factory()->create();

        /** @var CategoryLang $itCategoryLang */
        $itCategoryLang = CategoryLang::factory()->create([
            'category_id' => $category->id,
            'lang'        => 'it',
            'name'        => 'nome della categoria',
        ]);

        /** @var CategoryLang $enCategoryLang */
        $enCategoryLang = CategoryLang::factory()->create([
            'category_id' => $category->id,
            'lang'        => 'en',
            'name'        => 'category name',
        ]);

        /** @var CategoryService $categoryService */
        $categoryService = new CategoryService();

        /** @var Page $categoryPage */
        $categoryPage = $categoryService->storeCategoryPage($category);

        $this->assertDatabaseHas('pages', [
            'id'   => 1,
            'name' => 'nome della categoria',
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'              => 'categories/nome-della-categoria',
            'page_id'          => $categoryPage->id,
            'lang'             => $itCategoryLang->lang,
            'meta_title'       => $itCategoryLang->name,
            'meta_description' => $itCategoryLang->description,
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'              => 'categories/category-name',
            'page_id'          => $categoryPage->id,
            'lang'             => $enCategoryLang->lang,
            'meta_title'       => $enCategoryLang->name,
            'meta_description' => $enCategoryLang->description,
        ]);
    }
}
