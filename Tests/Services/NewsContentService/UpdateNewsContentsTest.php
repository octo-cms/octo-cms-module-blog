<?php

namespace OctoCmsModule\Blog\Tests\Services\NewsContentService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Services\NewsContentService;
use OctoCmsModule\Blog\Tests\Mocks\Services\PictureServiceMock;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use Throwable;

/**
 * Class SaveNewsTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\NewsService
 */
class UpdateNewsContentsTest extends TestCase
{



    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = [
            [
                ['id' => 1, 'position' => 1],
                ['id' => 2, 'position' => 2],
                ['id' => 3, 'position' => 3],
                ['id' => 4, 'position' => 4],
            ],
            [1, 2, 3, 4],
        ];

        $providers[] = [
            [['id' => 1, 'position' => 1], ['id' => 2, 'position' => 2], ['id' => 4, 'position' => 3]],
            [1, 2, 4],
        ];

        $providers[] = [
            [],
            [],
        ];

        $providers[] = [
            [
                ['id' => 1, 'position' => 1],
                ['id' => 2, 'position' => 2],
                ['id' => null, 'type' => 'html', 'position' => 3],
                ['id' => 4, 'position' => 4],
            ],
            [1, 2, 5, 4],
        ];

        return $providers;
    }

    /**
     * @param $fields
     * @param $results
     *
     * @throws OctoCmsException
     * @throws Throwable
     * @dataProvider dataProvider
     */
    public function test_updateNewsContents($fields, $results)
    {
        /** @var News $news */
        $news = News::factory()->create();

        NewsContent::factory()->count(4)->create([
            'news_id' => $news->id,
            'type'    => 'html',
        ]);

        /** @var NewsContentService */
        $newsContentService = new NewsContentService(
            new PictureServiceMock()
        );

        $newsContentService->updateNewsContents($news, $fields);

        $this->assertEquals(
            $news->newsContents()->pluck('id')->toArray(),
            $results
        );

    }

    /**
     * @throws OctoCmsException
     * @throws Throwable
     */
    public function test_updateNewsContentsPosition()
    {
        /** @var News $news */
        $news = News::factory()->create();

        /** @var NewsContent $htmlContent */
        $htmlContent = NewsContent::factory()->create([
            'news_id'  => $news->id,
            'type'     => 'html',
            'position' => 0,
        ]);

        /** @var NewsContent $pictureContent */
        $pictureContent = NewsContent::factory()->create([
            'news_id'  => $news->id,
            'type'     => 'picture',
            'position' => 1,
        ]);

        /** @var NewsContentService */
        $newsContentService = new NewsContentService(
            new PictureServiceMock()
        );

        $newsContentService->updateNewsContents($news, [
            [
                'id'       => $htmlContent->id,
                'type'     => 'html',
                'position' => 1,
            ],
            [
                'id'       => $pictureContent->id,
                'type'     => 'picture',
                'position' => 0,
            ],
        ]);

        $this->assertDatabaseHas('blog_news_contents', [
            'id'       => $htmlContent->id,
            'type'     => 'html',
            'position' => 1,
        ]);

        $this->assertDatabaseHas('blog_news_contents', [
            'id'       => $pictureContent->id,
            'type'     => 'picture',
            'position' => 0,
        ]);
    }
}
