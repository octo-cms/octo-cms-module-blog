<?php

namespace OctoCmsModule\Blog\Tests\Services\NewsContentService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Services\NewsContentService;
use OctoCmsModule\Blog\Tests\Mocks\Services\PictureServiceMock;
use ReflectionException;

/**
 * Class SaveNewsContentsDataTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\NewsContentService
 */
class SaveNewsContentsDataTest extends TestCase
{



    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = [NewsContent::TYPE_GALLERY, true];
        $providers[] = [NewsContent::TYPE_VIDEO, true];
        $providers[] = ['wrong type', false];

        return $providers;
    }

    /**
     * @param $type
     * @param $result
     *
     * @throws ReflectionException
     * @dataProvider dataProvider
     */
    public function test_updateNewsContents($type, $result)
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()->create([
            'type' => $type,
        ]);

        /** @var NewsContentService */
        $newsContentService = new NewsContentService(
            new PictureServiceMock()
        );

        $this->assertEquals(
            $result,
            $this->invokeMethod(
                $newsContentService,
                'saveNewsContentsData',
                [$newsContent, []]
            )
        );
    }
}
