<?php

namespace OctoCmsModule\Blog\Tests\Services\NewsContentService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\NewsContent;
use OctoCmsModule\Blog\Entities\NewsContentLang;
use OctoCmsModule\Blog\Services\NewsContentService;
use OctoCmsModule\Blog\Tests\Mocks\Services\PictureServiceMock;
use ReflectionException;

/**
 * Class SaveNewsContentDataHtmlTest
 *
 * @package OctoCmsModule\Blog\Tests\Services\NewsContentService
 */
class SaveNewsContentDataHtmlTest extends TestCase
{



    /**
     * @throws ReflectionException
     */
    public function test_saveNewsContentDataHtml()
    {
        /** @var NewsContent $newsContent */
        $newsContent = NewsContent::factory()
            ->has(NewsContentLang::factory()->state([
                'lang' => 'it',
                'text' => 'text-it',
            ]))
            ->create();

        /** @var NewsContentService */
        $newsContentService = new NewsContentService(
            new PictureServiceMock()
        );

        $this->invokeMethod(
            $newsContentService,
            'saveNewsContentDataHtml',
            [
                'newsContent' => $newsContent,
                'content'     => [
                    'newsContentLangs' => [
                        [
                            'lang' => 'it',
                            'text' => 'text-it-new',
                        ],
                        [
                            'lang' => 'en',
                            'text' => 'text-en',
                        ],
                    ],
                ],
            ]
        );

        $this->assertDatabaseHas(
            'blog_news_content_langs',
            [
                'news_content_id' => $newsContent->id,
                'lang'            => 'it',
                'text'            => 'text-it-new',
            ]
        );

        $this->assertDatabaseHas(
            'blog_news_content_langs',
            [
                'news_content_id' => $newsContent->id,
                'lang'            => 'en',
                'text'            => 'text-en',
            ]
        );
    }

    /**
     * @throws ReflectionException
     */
    public function test_saveNewsContentDataHtmlEmptyLang()
    {
        /** @var NewsContentService */
        $newsContentService = new NewsContentService(
            new PictureServiceMock()
        );

        $this->assertFalse($this->invokeMethod(
            $newsContentService,
            'saveNewsContentDataHtml',
            [
                'newsContent' => new NewsContent(),
                'content'     => [
                    'newsContentLangs' => [
                        [
                            'lang' => '',
                            'text' => '',
                        ],
                    ],
                ],
            ]
        ));
    }
}
