<?php

namespace OctoCmsModule\Blog\Tests\Controllers\CategoryController;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\Category;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\AttributeController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Category::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.categories.news');
    }


    public function test_datatableIndexQuery()
    {
        Sanctum::actingAs(self::createAdminUser());

        Category::factory()->count(15)->create();

        $response = $this->json(
            'POST',
            route('admin.datatables.categories.news'),
            [
                'currentPage' => 1,
                'rowsInPage'  => 10,
                'query'       => 'query',
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertEquals(15, Arr::get($content, 'total', 0));

        $this->assertEquals(1, Arr::get($content, 'currentPage', 0));

        $this->assertEquals(10, Arr::get($content, 'rowsInPage', 0));

        $this->assertNotEmpty(Arr::get($content, 'collection', []));
    }
}
