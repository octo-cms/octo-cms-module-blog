<?php

namespace OctoCmsModule\Blog\Tests\Controllers\CategoryController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\CategoryController
 */
class StoreTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'parent_id'     => null,
            'active'        => true,
            'categoryLangs' => [
                [
                    'lang'              => 'it',
                    'name'              => 'nome',
                    'description'       => 'descrizione',
                    'short_description' => 'descrizione breve',
                ],
            ],
            'tags'          => ['tag 1', 'tag 2'],
            'pictures'      => [],
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['active']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['parent_id']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['categoryLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['categoryLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['categoryLangs'][0]['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['categoryLangs'][0]['description']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['categoryLangs'][0]['description'] = null;
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['categoryLangs'][0]['short_description']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['categoryLangs'][0]['short_description'] = null;
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['tags']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['pictures']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('admin.blog.categories.store'),
            $fields
        );

        $response->assertStatus($status);
    }
}
