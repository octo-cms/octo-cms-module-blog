<?php

namespace OctoCmsModule\Blog\Tests\Controllers\CategoryController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\Category;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class DeleteTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\CategoryController
 */
class DeleteTest extends TestCase
{


    public function test_delete()
    {
        /** @var Category $category */
        $category = Category::factory()->create();

        /** @var Page $page */
        $page = Page::factory()->create([
            'type'          => Page::TYPE_CATEGORY_NEWS,
            'pageable_type' => Page::PAGEABLE_TYPE_CATEGORY_NEWS,
            'pageable_id'   => $category->id,
        ])->first();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('admin.blog.categories.delete', ['id' => $category->id])
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseHas('blog_categories', [
            'id' => $category->id,
        ]);

        $this->assertDatabaseHas('pages', [
            'id' => $page->id,
        ]);

        $this->assertNull(Category::find($category->id));
        $this->assertNull(Page::find($page->id));
    }
}
