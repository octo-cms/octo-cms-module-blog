<?php

namespace OctoCmsModule\Blog\Tests\Controllers\CategoryController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\Category;

/**
 * Class ShowTreeTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\CategoryController
 */
class ShowTreeTest extends TestCase
{


    public function test_showTree()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Category $parent1 */
        $parent1 = Category::factory()
            ->has(Category::factory()->count(2), 'children')
            ->create();

        /** @var Category $child */
        $child = Category::factory()->create([
            'parent_id' => $parent1->id,
        ]);

        Category::factory()->create([
            'parent_id' => $child->id,
        ]);

        Category::factory()
            ->has(Category::factory()->count(2), 'children')
            ->create();

        Category::factory()
            ->has(Category::factory()->count(2), 'children')
            ->create();

        $response = $this->json(
            'GET',
            route('admin.blog.categories.show-tree')
        );

        $content = json_decode($response->getContent(), true);

        $this->assertCount(3, $content['data']);

        $this->assertCount(3, $content['data'][0]['children']);
        $this->assertCount(2, $content['data'][1]['children']);
        $this->assertCount(2, $content['data'][2]['children']);

        $this->assertCount(1, $content['data'][0]['children'][2]['children']);
    }
}
