<?php

namespace OctoCmsModule\Blog\Tests\Controllers\CategoryController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Blog\Entities\CategoryLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SelectIndexTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\CategoryController
 */
class SelectIndexTest extends TestCase
{


    public function test_selectIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        CategoryLang::factory()->create([
            'name' => 'test-name',
            'lang' => 'it',
        ]);

        $response = $this->json(
            'POST',
            route('admin.select.categories.news.index'),
            ['query' => 'test',]
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'name' => 'test-name',
        ]);

    }
}
