<?php

namespace OctoCmsModule\Blog\Tests\Controllers\NewsController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;


/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\NewsController
 */
class StoreTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            'active'     => true,
            'author'     => 'author',
            'date'       => '1974-01-01',
            'newsLangs'  => [
                [
                    'lang'              => 'it',
                    'title'             => 'titolo',
                    'short_description' => 'descrizione breve',
                ],
            ],
            'tags'       => ['tag 1', 'tag 2'],
            'categories' => [
                ['id' => 1, 'main' => true]
            ],
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['active']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['newsLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['newsLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['newsLangs'][0]['title']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['newsLangs'][0]['short_description']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['tags']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['categories']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['categories'][0]['id']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['categories'][0]['main']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('admin.blog.news.store'),
            $fields
        );

        $response->assertStatus($status);
    }

}
