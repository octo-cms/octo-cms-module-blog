<?php

namespace OctoCmsModule\Blog\Tests\Controllers\NewsController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\NewsController
 */
class UpdateNewsContentsTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            [
                'id'               => null,
                'type'             => 'html',
                'position'         => 0,
                'newsContentLangs' => [],
                'pictures'         => [],
                'videos'           => [],
            ],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_updateContents(array $fields, int $status)
    {
        /** @var News $news */
        $news = News::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('admin.blog.news.update.contents', ['id' => $news->id]),
            $fields
        );

        $response->assertStatus($status);
    }
}
