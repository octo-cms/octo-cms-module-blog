<?php

namespace OctoCmsModule\Blog\Tests\Controllers\NewsController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class DeleteTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\NewsController
 */
class DeleteTest extends TestCase
{


    public function test_delete()
    {
        /** @var News $news */
        $news = News::factory()->create();

        Page::factory()->create([
            'pageable_type' => Page::PAGEABLE_TYPE_NEWS,
            'pageable_id'   => $news->id,
        ]);

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('admin.blog.news.delete', ['id' => $news->id])
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('blog_news', [
            'id' => $news->id,
        ]);

        $this->assertEmpty($news->page()->get());
    }
}
