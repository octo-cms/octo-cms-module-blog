<?php

namespace OctoCmsModule\Blog\Tests\Controllers\NewsController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;

/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\NewsController
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        /** @var News $news */
        $news = News::factory()->has(NewsLang::factory()->count(2))->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('admin.blog.news.show', ['id' => $news->id])
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'id'     => $news->id,
            'active' => $news->active,
            'author' => $news->author,
        ]);
    }
}
