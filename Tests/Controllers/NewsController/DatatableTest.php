<?php

namespace OctoCmsModule\Blog\Tests\Controllers\NewsController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Blog\Entities\News;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Blog\Tests\Controllers\NewsController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        News::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.news');
    }
}
