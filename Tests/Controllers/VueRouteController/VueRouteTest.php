<?php

namespace OctoCmsModule\Blog\Tests\Controllers\VueRouteController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class VueRouteTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Blog\Tests\Controllers\VueRouteController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteTest extends TestCase
{


    /**
     * Name dataProvider
     *
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = ['admin.vue-route.blog.news'];
        $providers[] = ['admin.vue-route.blog.categories'];
        $providers[] = ['admin.vue-route.blog.settings'];
        $providers[] = ['admin.vue-route.blog.news-builder', ['id' => 1]];

        return $providers;
    }

    /**
     * Name test_routes
     *
     * @dataProvider dataProvider
     * @param string $route
     * @param array  $query
     * @return void
     */
    public function test_routes(string $route, array $query = [])
    {
        $path = !empty($query) ? route($route, $query) : route($route);

        Sanctum::actingAs(self::createAdminUser());
        $this->withoutMix();
        $response = $this->json( 'GET', $path );

        $response->assertStatus(Response::HTTP_OK);

    }
}
