<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->string('author', 100);
            $table->timestamp('date')->index();
            $table->timestamps();
        });

        Schema::create('blog_category_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('news_id')->unsigned();
            $table->boolean('main')->default(true);

            $table->foreign('category_id')->references('id')
                ->on('blog_categories')->onDelete('cascade');

            $table->foreign('news_id')->references('id')
                ->on('blog_news')->onDelete('cascade');
        });

        Schema::create('blog_news_related', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('news_id')->unsigned();
            $table->bigInteger('related_id')->unsigned();

            $table->foreign('news_id')->references('id')
                ->on('blog_news')->onDelete('cascade');

            $table->foreign('related_id')->references('id')
                ->on('blog_news')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_news');
    }
}
