<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_category_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->index()->unsigned();
            $table->string('lang', 10);
            $table->string('name');
            $table->string('slug')->index();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();

            $table->foreign('category_id')->references('id')
                ->on('blog_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_category_langs');
    }
}
