<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsContentLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_news_content_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('news_content_id')->unsigned();
            $table->string('lang', 5);
            $table->text('text')->nullable();


            $table->foreign('news_content_id')->references('id')
                ->on('blog_news_contents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_news_content_langs');
    }
}
