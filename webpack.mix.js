const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

require('laravel-mix-alias');
mix.alias({
    '@octoCmsComponents': '../Admin/Resources/octo-cms-components',
    '@adminNodeModules': '../Admin/node_modules',
    '@siteBuilderVue': '../Sitebuilder/Resources/vue',
});

const assetPath = '../../public/assets-' + process.env.MIX_ADMIN_PREFIX;

const assetVuePath = assetPath + '/vue/blog/'

mix.js(__dirname + '/Resources/vue/views/news.js', assetVuePath + 'news.min.js');
mix.js(__dirname + '/Resources/vue/views/category.js', assetVuePath + 'category.min.js');
mix.js(__dirname + '/Resources/vue/views/newsBuilder.js', assetVuePath + 'news-builder.min.js');
mix.js(__dirname + '/Resources/vue/views/settings.js', assetVuePath + 'settings.min.js');

if (mix.inProduction()) {
    mix.version();
}
