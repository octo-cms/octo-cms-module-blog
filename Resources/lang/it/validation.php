<?php

return [
    'active'        => [
        'required' => 'Il campo active è obbligatorio.',
        'boolean'  => 'Il campo active deve essere un booleano.',
    ],
    'categoryLangs' => [
        'required' => 'Le traduzioni sono obbligatorie.',
        '*'        => [
            'lang' => [
                'required' => 'La lingua è obbligatorie.',
                'string'   => 'La lingua deve essere una stringa.',
            ],
            'name' => [
                'required' => 'Il campo name è obbligatorio.',
                'string'   => 'Il campo name deve essere una stringa.',
            ],
        ],
    ],
    'author'        => [
        'required' => 'Il campo author è obbligatorio.',
        'string'   => 'Il campo author deve essere un string.',
    ],
    'date'          => [
        'required' => 'Il campo date è obbligatorio.',
    ],
    'newsLangs'     => [
        'required' => 'Le traduzioni sono obbligatorie.',
        '*'        => [
            'lang'              => [
                'required' => 'La lingua è obbligatorie.',
                'string'   => 'La lingua deve essere una stringa.',
            ],
            'title'             => [
                'required' => 'Il campo title è obbligatorio.',
                'string'   => 'Il campo title deve essere una stringa.',
            ],
            'short_description' => [
                'required' => 'Il campo short description è obbligatorio.',
                'string'   => 'Il campo short description deve essere una stringa.',
            ],
        ],
    ],
];
