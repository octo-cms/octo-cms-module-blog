@extends($template . '::layouts.master')

@section('content')

    <x-single-news-component :page-lang="$pageLang"/>

    @if(!empty($contents))
        @foreach($contents as $content)
            <x-page-content-handler :content="$content"/>
        @endforeach
    @endif
@endsection
