import NewsContentHtml from "../components/newsBuilder/NewsContentHtml";
import NewsContentPicture from "../components/newsBuilder/NewsContentPicture";
// import NewsContentGallery from "@/modules/Blog/components/newsBuilder/NewsContentGallery";
// import NewsContentVideo from "@/modules/Blog/components/newsBuilder/NewsContentVideo";

const newsContentConst = {
    html : {
        icon : 'coding',
        label: 'HTML',
        component : NewsContentHtml
    },
    picture : {
        icon : 'image',
        label: 'Immagine',
        component : NewsContentPicture
    },
    // gallery: {
    //     icon: 'slider',
    //     label: 'Gallery',
    //     component : NewsContentGallery
    // },
    // video: {
    //     icon: 'coding',
    //     label: 'Video',
    //     component : NewsContentVideo
    // }
};

export default newsContentConst
