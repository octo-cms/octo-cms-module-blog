import Page from "@siteBuilderVue/models/page";

const News = {
    id: null,
    active: true,
    author: '',
    data: null,
    newsLangs: [],
    page: Page,
    newsContents: [],
    pictures: [],
    tags: [],
    categories: [],
};

export default News
