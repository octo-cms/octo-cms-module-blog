const NewsContent = {
    id: null,
    news_id: null,
    position: 0,
    type: '',
    newsContentLangs: [],
    pictures: [],
    videos: [],
};

export default NewsContent
