const CategoryLang = {
    id: null,
    category_id: null,
    name: '',
    description: '',
    short_description: '',
};

export default CategoryLang
