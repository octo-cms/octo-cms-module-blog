const Category = {
    id: null,
    parent_id: null,
    level_depth: 0,
    active: true,
    categoryLangs: [],
    children: [],
    parents: {},
    tags: [],
    pictures: [],
};

export default Category
