const routes = {

    SELECT: {
        categoriesNews: 'api/admin/v1/select/categories-news',
    },

    DATATABLES: {
        news: 'api/admin/v1/datatables/blog/news',
        categoriesNews: 'api/admin/v1/datatables/blog/categories',
    },

    NEWS_STORE: 'api/admin/v1/blog/news',
    NEWS_SHOW: 'api/admin/v1/blog/news/{id}',
    NEWS_UPDATE: 'api/admin/v1/blog/news/{id}',
    NEWS_DELETE: 'api/admin/v1/blog/news/{id}',
    NEWS_CONTENT_UPDATE: 'api/admin/v1/blog/news/{id}/contents',
    CATEGORIES_SHOW_TREE: 'api/admin/v1/blog/categories/show-tree',
    CATEGORIES_STORE: 'api/admin/v1/blog/categories/',
    CATEGORIES_UPDATE: 'api/admin/v1/blog/categories/{id}',
    CATEGORIES_DELETE: 'api/admin/v1/blog/categories/{id}',

    CONFIG_SETTINGS_POST: 'api/admin/v1/settings',

    NEWS_BUILDER_VIEW: 'blog/news-builder/{id}',

};

export {
    routes
}
